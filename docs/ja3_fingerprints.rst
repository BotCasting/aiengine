JA3 Fingerprints
~~~~~~~~~~~~~~~~

The engine is capable of computing the JA3 TLS Fingerprints (https://github.com/salesforce/ja3) on the Client Hello for detect malware on TLS connections.
In order to use this feature you need to use the --enable-ja3 on the configure script.

We will use two examples of how to combine the signatures with the engine.

First we load the signatures on a dict() object. The signatures used are form sslbl.abuse.ch

.. code:: python

   def load_fingerprints():

       # The file has been download from https://sslbl.abuse.ch/blacklist/ja3_fingerprints.rules
       with open("ja3_fingerprints.rules") as fja3:
           for line in fja3.readlines():
               if (not line.startswith("#")):
                   l = line.strip()
                   a = re.split("\(msg:", l)
                   # Parse the line
                   name = a[1].split("\"")[1]
                   value = a[1].split("content:")[1].split(";")[0].replace("\"","")
                   fingerprints[value] = name

       return fingerprints

The first detection method is by using a specific SSL callback and label the flow with the name of the malware

.. code:: python

   def callback_ssl(flow):

       d = flow.ssl_info
       if (d):
           f = d.fingerprint
           if f in fingerprints:
               malware_name = fingerprints[f] 
               print("WARNING: Malware '%s' on '%s'" % (malware_name, flow)) 
               flow.label = malware_name


If we use a database adaptor for logging we will have something like:

.. code:: json

   {
       "anomaly": 14,
       "bytes": 126616,
       "info": {
           "cipher": 47,
           "fingerprint": "1d095e68489d3c535297cd8dffb06cb9",
           "host": "fillizee.com",
           "issuer": "foror2",
           "matchs": "All .com",
           "pdus": 4,
           "tcpflags": "Flg[S(1)SA(1)A(158)F(0)R(0)P(8)Seq(3382182658,2427977064)]",
           "version": 769
       },
       "ip": {
           "dst": "5.135.252.103",
           "src": "10.9.6.103"
       },
       "label": "SSLBL: Malicious JA3 SSL-Client Fingerprint detected (Tofsee)",
       "layer7": "ssl",
       "port": {
           "dst": 443,
           "src": 49165
       },
       "proto": 6
   }

The second method is to do the detection on the database adaptor, basically is move the code of the callback to the update method of the adaptor

.. code:: python

   class databaseAdaptor (pyaiengine.DatabaseAdaptor):
       def __init__(self):
           self.__detections = 0

       def update(self, key, data):
           jdata = json.loads(data)
           if (jdata["layer7"] == "ssl"]):
               f = jdata["info"]["fingerprint"]
                if (f):
                    if f in fingerprints:
                        print("WARNING: Malware '%s' on '%s'" % (fingerprints[f], key))

