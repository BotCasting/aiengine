#!/usr/bin/env python

""" Example for detect bitcoinminer on the network """

__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright (C) 2013-2020 by Luis Campo Giralte"
__revision__ = "$Id$"
__version__ = "0.1"

import sys
from pyaiengine import *
sys.path.append("../src/")

def callback(flow):

    print("Detected Bitcoinminer on ip:", flow.srcip)

if __name__ == '__main__':

    # Load an instance of a Lan Stack
    stack = StackLan()

    rman = RegexManager()

    reg = Regex("First regex", "mining.subscribe",
                Regex("Second regex", "c4758493e4f9804beeb784b4ff0be019b03678952ea8bb6f5c5365b2b76438a7"))

    reg.next_regex.callback = callback
    rman.add_regex(reg)

    stack.tcp_regex_manager = rman

    stack.tcp_flows = 327680
    stack.udp_flows = 163840

    with PacketDispatcher("/home/luis/pcapfiles/bitcoinminer.pcap") as pd:
        pd.stack = stack
        pd.run()

    stack.show_flows()
    # Dump on file the statistics of the stack
    stack.stats_level = 5
    with open("statistics.log", "w") as file:
        file.write(str(stack))

    sys.exit(0)

