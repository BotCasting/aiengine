/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "UDPGenericProtocol.h"
#include <iomanip> // setw

namespace aiengine {

bool UDPGenericProtocol::check(const Packet &packet) {

	setHeader(packet.getPayload());
	++total_valid_packets_;
	return true;
}

void UDPGenericProtocol::processFlow(Flow *flow) {

        ++total_packets_;
        total_bytes_ += flow->packet->getLength();

	++flow->total_packets_l7;

	// TODO: The behaviour of UDP is different than TCP, in the future this function will
	// be different than the TCPGenericProtocol.

	if (flow->regex_mng) {
                const uint8_t *payload = flow->packet->getPayload();
		boost::string_ref data(reinterpret_cast<const char*>(payload), flow->packet->getLength());

		eval_.processFlowPayloadLayer7(flow, data);
	}
}

void UDPGenericProtocol::setRegexManager(const SharedPointer<RegexManager> &rm) {

        if (rm_)
                rm_->setPluggedToName("");
       
	if (rm) {
        	rm_ = rm;
        	rm_->setPluggedToName(getName());
	} else {
		rm_.reset();
	}
}

void UDPGenericProtocol::statistics(std::basic_ostream<char> &out, int level) const {

	showStatisticsHeader(out, level);

	if (level > 5)
		if (flow_forwarder_.lock())
			flow_forwarder_.lock()->statistics(out);
	if (level > 3)
		if (rm_)
			rm_->statistics(out);
}

void UDPGenericProtocol::statistics(Json &out, int level) const {          

	showStatisticsHeader(out, level);
}

CounterMap UDPGenericProtocol::getCounters() const {
  	CounterMap cm; 

        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);

        return cm;
}

void UDPGenericProtocol::resetCounters() {

	reset();

}

} // namespace aiengine
