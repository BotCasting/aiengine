/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "SMTPInfo.h"

namespace aiengine {

void SMTPInfo::reset() { 
	resetStrings();
	command_ = 0;
	is_banned_ = false; 
	is_data_ = false; 
	is_starttls_ = false; 
	total_data_bytes_ = 0;
	total_data_blocks_ = 0;
}

void SMTPInfo::resetStrings() { 

	matched_domain_name.reset();
	from.reset(); 
	to.reset(); 
}

std::ostream& operator<< (std::ostream &out, const SMTPInfo &info) {

	if (info.isBanned()) 
		out << " Banned";

	if (info.from)  
		out << " From:" << info.from->getName();

	if (info.to) 
		out << " To:" << info.to->getName();

	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const SMTPInfo &info) {

       	out["total"] = info.total_data_blocks_;
       	out["bytes"] = info.total_data_bytes_;

	if (info.isBanned()) 
		out["banned"] = true;

	if (info.from)  
		out["from"] = info.from->getName();

	if (info.to) 
		out["to"] = info.to->getName();

	if (info.is_starttls_)
        	out["tls"] = true;

	return out;
}

} // namespace aiengine
