/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "VLanProtocol.h"
#include <iomanip>

namespace aiengine {

bool VLanProtocol::check(const Packet &packet){

	int length = packet.getLength();

	if (length >= header_size) {
		setHeader(packet.getPayload());

		++total_valid_packets_;
		return true;
	} else {
		++total_invalid_packets_;
		return false;
        }
}

bool VLanProtocol::processPacket(Packet &packet) {

	++total_packets_;
	total_bytes_ += packet.getLength();

	if (!mux_.expired()) {
        	MultiplexerPtr mux = mux_.lock();

                mux->setNextProtocolIdentifier(getEthernetType());

                // Sets the Tag for the packet
                packet.setTag(getVlanId());
                
		mux->setHeaderSize(header_size);
                packet.setPrevHeaderSize(header_size);
        }
	return true;
}

void VLanProtocol::statistics(std::basic_ostream<char>& out, int level) const {

	showStatisticsHeader(out, level);

	if (level > 5)
		if (mux_.lock())
			mux_.lock()->statistics(out);
}

void VLanProtocol::statistics(Json &out, int level) const {

	showStatisticsHeader(out, level);
}

CounterMap VLanProtocol::getCounters() const {
	CounterMap cm;

        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);

        return cm;
}

void VLanProtocol::resetCounters() {

	reset();
}

} // namespace aiengine
