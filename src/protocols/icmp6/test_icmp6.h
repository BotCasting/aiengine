/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef _TEST_ICMP6_H_
#define _TEST_ICMP6_H_

#include <string>
#include "Protocol.h"
#include "StackTest.h"
#include "../vlan/VLanProtocol.h"
#include "../ip6/IPv6Protocol.h"
#include "ICMPv6Protocol.h"

using namespace aiengine;

struct StackIcmp6 : public StackTest
{
        IPv6ProtocolPtr ip6;
        ICMPv6ProtocolPtr icmp6;
        MultiplexerPtr mux_ip;
        MultiplexerPtr mux_icmp;

	StackIcmp6() 
	{
		ip6 = IPv6ProtocolPtr(new IPv6Protocol());
		icmp6 = ICMPv6ProtocolPtr(new ICMPv6Protocol());
		mux_ip = MultiplexerPtr(new Multiplexer());
		mux_icmp = MultiplexerPtr(new Multiplexer());

		// configure the ip
		ip6->setMultiplexer(mux_ip);
		mux_ip->setProtocol(static_cast<ProtocolPtr>(ip6));

		//configure the icmp
		icmp6->setMultiplexer(mux_icmp);
		mux_icmp->setProtocol(static_cast<ProtocolPtr>(icmp6));

        	// configure the multiplexers
        	mux_eth->addUpMultiplexer(mux_ip);
        	mux_ip->addDownMultiplexer(mux_eth);
        	mux_ip->addUpMultiplexer(mux_icmp);
        	mux_icmp->addDownMultiplexer(mux_ip);
	}

	~StackIcmp6() {}
};

#endif
