/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "RTPProtocol.h"
#include <iomanip>

namespace aiengine {

RTPProtocol::~RTPProtocol() {

	anomaly_.reset();
}

bool RTPProtocol::check(const Packet &packet) {

	int length = packet.getLength();

	if (length >= header_size) {
		setHeader(packet.getPayload());
		if ((header_->version & (1 << 7))and((header_->version & (1 << 6)) == 0)) {
			++total_valid_packets_;
			return true;
		}
	}
	++total_invalid_packets_;
	return false;
}

int64_t RTPProtocol::getAllocatedMemory() const {

        int64_t mem = sizeof(RTPProtocol);

        return mem;
}

int64_t RTPProtocol::getTotalAllocatedMemory() const {

	return getAllocatedMemory();
}

uint8_t RTPProtocol::getPayloadType() const { 

	uint8_t pt = header_->payload_type;
	
	// Check if the first bit is set
	if (((pt) & (1 << 7)) != 0) 
		pt &= ~(1 << 7); // Sets the first bit to zero

	return pt;
}

void RTPProtocol::processFlow(Flow *flow) {

	int length = flow->packet->getLength();
	total_bytes_ += length;
	++total_packets_;
	++flow->total_packets_l7;

	current_flow_ = flow;

	if (length >= header_size) {
		setHeader(flow->packet->getPayload());
		if (header_->version == 0x80) {
			// TODO, process for zrtp?
		}
	} else {
                if (flow->getPacketAnomaly() == PacketAnomalyType::NONE)
                        flow->setPacketAnomaly(PacketAnomalyType::RTP_BOGUS_HEADER);

                anomaly_->incAnomaly(PacketAnomalyType::RTP_BOGUS_HEADER);
	}
}

void RTPProtocol::statistics(std::basic_ostream<char>& out, int level) const { 

	showStatisticsHeader(out, level);

	if (level > 5)
		if (flow_forwarder_.lock())
			flow_forwarder_.lock()->statistics(out);
}

void RTPProtocol::statistics(Json &out, int level) const {

	showStatisticsHeader(out, level);
}

CounterMap RTPProtocol::getCounters() const {
       	CounterMap cm;
 
        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);

        return cm;
}

void RTPProtocol::resetCounters() {

	reset();
}

} // namespace aiengine
