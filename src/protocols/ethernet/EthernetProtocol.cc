/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "EthernetProtocol.h"
#include <iomanip> // setw

namespace aiengine {

EthernetProtocol::EthernetProtocol(const std::string &name):
	Protocol(name) {}

bool EthernetProtocol::check(const Packet &packet) {

	const int length = packet.getLength();

	// max_ethernet_len_ on network devices shoudl be equal to the mtu
	// but on pcap/pcapng files there is no limit of packet size
	if ((length >= 20 + 14) and (length <= max_ethernet_len_)) {
		const uint8_t *pkt = packet.getPayload();
		setHeader(pkt);

		packet.link_packet.setPayload(pkt);
		packet.link_packet.setLength(length);
		++total_valid_packets_;
		// Normaly we increase the total_packets on the processPacket function
		// However this is a special case due to the ethernet is close the PacketDispatcher

		++total_packets_;
		total_bytes_ += length;
		return true;
	} else {
		++total_invalid_packets_;
		return false;
	}
}


// Just used when there is ethernet on the middle part of a stack
// Check vxlan for further details

bool EthernetProtocol::processPacket(Packet &packet) {

	if (MultiplexerPtr mux = mux_.lock(); mux) {
                mux->setNextProtocolIdentifier(getEthernetType());

                mux->setHeaderSize(header_size);
		packet.link_packet.setPayload(packet.getPayload());
		packet.link_packet.setLength(packet.getLength());
                packet.setPrevHeaderSize(header_size);
	}
	return true;
}

void EthernetProtocol::statistics(std::basic_ostream<char> &out, int level) const {

	showStatisticsHeader(out, level);

	if (level > 5)
		if (mux_.lock())
			mux_.lock()->statistics(out);
}

void EthernetProtocol::statistics(Json &out, int level) const {

	showStatisticsHeader(out, level);
}

CounterMap EthernetProtocol::getCounters() const {
	CounterMap cm;

	cm.addKeyValue("packets", total_packets_);
	cm.addKeyValue("bytes", total_bytes_);

        return cm;
}

void EthernetProtocol::resetCounters() {

	reset();
}

} // namespace aiengine
