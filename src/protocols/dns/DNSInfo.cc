/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "DNSInfo.h"

namespace aiengine {

void DNSInfo::reset() { 

	name.reset() ; 
	qtype_ = 0; 
	items_.clear(); 
	matched_domain_name.reset(); 
	is_banned_ = false;
}

void DNSInfo::addIPAddress(const char* ipstr) { 
	
	items_.emplace_back(ipstr); 
}

void DNSInfo::addName(const char* name) { 
	
	items_.emplace_back(name); 
}

void DNSInfo::addName(const char *name, int length) {

	items_.emplace_back(name, length);
}

std::ostream& operator<< (std::ostream &out, const DNSInfo &info) {

	if (info.isBanned()) 
		out << " Banned";

	if (info.name) 
		out << " Domain:" << info.name->getName();

	return out;
}

nlohmann::json& operator<< (nlohmann::json &out, const DNSInfo &info) {

	out["qtype"] = info.qtype_;

	if (info.isBanned()) 
		out["banned"] = true;

	if (info.name) 
		out["domain"] = info.name->getName();

	if (info.items_.size() > 0) 
		out["ips"] = info.items_;

        if (info.matched_domain_name)
                out["matchs"] = info.matched_domain_name->getName();

	return out;
}

} // namespace aiengine
