/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "MQTTProtocol.h"
#include <iomanip> // setw

namespace aiengine {

// List of support operations
std::vector<MqttControlPacketType> MQTTProtocol::commands_ {
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_RESERVED1),	"reserveds",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_CONNECT),		"connects",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_CONNACK),		"connects ack",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PUBLISH),		"publishs",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PUBACK),		"publishs ack",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PUBREC),		"publishs rec",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PUBREL),		"publishs rel",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PUBCOMP),		"publishs comp",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_SUBSCRIBE),	"subscribes",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_SUBACK),		"subscribes ack",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_UNSUBSCRIBE),	"unsubscribes",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_UNSUBACK),		"unsubscribes ack",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PINGREQ),		"pings req",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PINGRESP),		"pings res",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_DISCONNECT),	"disconnects",	0),
	std::make_tuple(static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_RESERVED2),	"reserveds",	0)
};

MQTTProtocol::MQTTProtocol():
	Protocol("MQTT", IPPROTO_TCP) {}

MQTTProtocol::~MQTTProtocol() {

	anomaly_.reset();
}

bool MQTTProtocol::check(const Packet &packet) {

	int length = packet.getLength();

	if (length >= header_size) {
		const uint8_t *payload = packet.getPayload();
		setHeader(payload);
		if (getCommandType() == static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_CONNECT)) {
			if (length >= header_size + (int)sizeof(mqtt_connect_header)) {
				const mqtt_connect_header *conn_hdr = reinterpret_cast<const mqtt_connect_header*>(&payload[header_size]);

				if((conn_hdr->proto_name[0] == 'M')and(conn_hdr->proto_name[1] == 'Q')) {
					++total_valid_packets_;
					return true;
				}
			}
		}
	}
	++total_invalid_packets_;
	return false;
}

int32_t MQTTProtocol::getLength() {

	// Specific way of manage the lengths
	if (header_->length >= 0x80) {
		int8_t tok = header_->data[0];
		if ((tok & 0x80) == 0) { // For two bytes
			int8_t val = (header_->length & 0x7F);
			int16_t value = val + (128 * tok);
			length_offset_ = 2;
			return value;
		}
	}
	length_offset_ = 1;
	return header_->length;
}

void MQTTProtocol::setDynamicAllocatedMemory(bool value) {

	info_cache_->setDynamicAllocatedMemory(value);
	topic_cache_->setDynamicAllocatedMemory(value);
}

bool MQTTProtocol::isDynamicAllocatedMemory() const {

	return info_cache_->isDynamicAllocatedMemory();
}

int64_t MQTTProtocol::getCurrentUseMemory() const {

	int64_t mem = sizeof(MQTTProtocol);

	mem += info_cache_->getCurrentUseMemory();
	mem += topic_cache_->getCurrentUseMemory();

	return mem;
}

int64_t MQTTProtocol::getAllocatedMemory() const {

	int64_t mem = sizeof(MQTTProtocol);

        mem += info_cache_->getAllocatedMemory();
        mem += topic_cache_->getAllocatedMemory();

        return mem;
}

int64_t MQTTProtocol::getTotalAllocatedMemory() const {

        int64_t mem = getAllocatedMemory();

	mem += compute_memory_used_by_maps();

	return mem;
}

int64_t MQTTProtocol::compute_memory_used_by_maps() const {

	int64_t bytes = 0;

	std::for_each (topic_map_.begin(), topic_map_.end(), [&bytes] (PairStringCacheHits const &f) {
		bytes += f.first.size();
	});
	return bytes;
}

int32_t MQTTProtocol::getTotalCacheMisses() const {

	int32_t miss = 0;

	miss = info_cache_->getTotalFails();
	miss += topic_cache_->getTotalFails();

	return miss;
}

void MQTTProtocol::releaseCache() {

	if (FlowManagerPtr fm = flow_mng_.lock(); fm) {
		auto ft = fm->getFlowTable();

		std::ostringstream msg;
        	msg << "Releasing " << getName() << " cache";

		infoMessage(msg.str());

		int64_t total_cache_bytes_released = compute_memory_used_by_maps();
		int64_t total_bytes_released_by_flows = 0;
		int64_t total_cache_save_bytes = 0;
		int32_t release_flows = 0;

                for (auto &flow: ft) {
                       	if (SharedPointer<MQTTInfo> info = flow->getMQTTInfo(); info) {
                                total_bytes_released_by_flows += sizeof(info);

                                flow->layer7info.reset();
                                ++release_flows;
                                info_cache_->release(info);
                        }
                }
                // Some entries can be still on the maps and needs to be
                // retrieve to their existing caches
                for (auto &entry: topic_map_) {
			total_cache_save_bytes += entry.second.sc->getNameSize() * (entry.second.hits - 1);
                        releaseStringToCache(topic_cache_, entry.second.sc);
		}
		topic_map_.clear();

                msg.str("");
                msg << "Release " << release_flows << " flows";
		computeMemoryUtilization(msg, total_cache_bytes_released, total_bytes_released_by_flows, total_cache_save_bytes);
                infoMessage(msg.str());
	}
}

void MQTTProtocol::releaseFlowInfo(Flow *flow) {

	if (auto info = flow->getMQTTInfo(); info)
		info_cache_->release(info);
}

void MQTTProtocol::processFlow(Flow *flow) {

	int length = flow->packet->getLength();
	const uint8_t *payload = flow->packet->getPayload();
	total_bytes_ += length;
	++total_packets_;

       	SharedPointer<MQTTInfo> info = flow->getMQTTInfo();

       	if (!info) {
               	if (info = info_cache_->acquire(); !info) {
			logFailCache(info_cache_->getName(), flow);
			return;
               	}
        	flow->layer7info = info;
	}

	current_flow_ = flow;

	if (info->getHaveData() == true) {
		int32_t left_length = info->getDataChunkLength() - length;
		if (left_length > 0) {
			info->setDataChunkLength(left_length);
		} else {
			info->setDataChunkLength(0);
			info->setHaveData(false);
		}
		return;
	}

	if (length >= header_size) {
		setHeader(payload);

		int8_t type = (int)getCommandType();
		if ((type > static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_RESERVED1))
			and(type < static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_RESERVED2))) {

			auto &command = commands_[type];

			int32_t *hits = &std::get<2>(command);
                        ++(*hits);
			info->setCommand(type);

			if (flow->getFlowDirection() == FlowDirection::FORWARD) { // client side
				++total_mqtt_client_commands_;
				info->incClientCommands();

				// The getLength also update the header_size with the variable length_offset_
				if (getLength() > length - header_size) {
					info->setDataChunkLength(getLength() - (length + header_size));
					info->setHaveData(true);
				}

				// The message publish message contains the topic and the information
				if (type == static_cast<int8_t>(MQTTControlPacketTypes::MQTT_CPT_PUBLISH))
					handle_publish_message(info.get(), &payload[header_size], length - header_size);

			} else { // Server side
				++total_mqtt_server_responses_;
				info->incServerCommands();
			}
		}
	}
	return;
}

void MQTTProtocol::handle_publish_message(MQTTInfo *info, const uint8_t *payload, int length) {

	int16_t msglen = 0;

	if (length_offset_ == 2)
		msglen = ntohs((payload[2] << 8) + payload[1]);
	else
		msglen = payload[1];

	if (msglen < length) {
		boost::string_ref topic((char*)&payload[length_offset_ + 1], msglen);

		attach_topic(info, topic);
	} else {
		++total_events_;
                if (current_flow_->getPacketAnomaly() == PacketAnomalyType::NONE)
                        current_flow_->setPacketAnomaly(PacketAnomalyType::MQTT_BOGUS_HEADER);

                anomaly_->incAnomaly(PacketAnomalyType::MQTT_BOGUS_HEADER);
	}
}

void MQTTProtocol::attach_topic(MQTTInfo *info, const boost::string_ref &topic) {

        if (!info->topic) {
                if (GenericMapType::iterator it = topic_map_.find(topic); it != topic_map_.end()) {
                        ++(it->second).hits;
                        info->topic = (it->second).sc;
		} else {
                        if (SharedPointer<StringCache> topic_ptr = topic_cache_->acquire(); topic_ptr) {
                                topic_ptr->setName(topic.data(), topic.size());
                                info->topic = topic_ptr;
                                topic_map_.insert(std::make_pair(topic_ptr->getName(), topic_ptr));
                        }
                }
        }
}

void MQTTProtocol::statistics(std::basic_ostream<char> &out, int level) const {

	std::ios_base::fmtflags f(out.flags());

	showStatisticsHeader(out, level);

	if (level > 3) {
		out << "\t" << "Total client commands:  " << std::setw(10) << total_mqtt_client_commands_ << "\n";
		out << "\t" << "Total server responses: " << std::setw(10) << total_mqtt_server_responses_ << "\n";

		for (auto &command: commands_) {
			const char *label = std::get<1>(command);
			int32_t hits = std::get<2>(command);
			out << "\t" << "Total " << label << ":" << std::right << std::setfill(' ') << std::setw(27 - strlen(label)) << hits << "\n";
		}
		out.flush();
	}
	if (level > 5)
		if (flow_forwarder_.lock())
			flow_forwarder_.lock()->statistics(out);
	if (level > 3) {
		info_cache_->statistics(out);
		topic_cache_->statistics(out);
		if (level > 4)
			showCacheMap(out, "\t", topic_map_, "MQTT Topics", "Topic");
	}
	out.flags(f); // Restore the out
}

void MQTTProtocol::statistics(Json &out, int level) const {

	showStatisticsHeader(out, level);

        if (level > 3) {
		out["client_commands"] = total_mqtt_client_commands_;
		out["server_commands"] = total_mqtt_server_responses_;

		Json j;

		for (auto &command: commands_)
			j.emplace(std::get<1>(command), std::get<2>(command));

		out["commands"] = j;
        }
}

void MQTTProtocol::increaseAllocatedMemory(int value) {

	info_cache_->create(value);
	topic_cache_->create(value);
}

void MQTTProtocol::decreaseAllocatedMemory(int value) {

	info_cache_->destroy(value);
	topic_cache_->destroy(value);
}

CounterMap MQTTProtocol::getCounters() const {
	CounterMap cm;

        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);
        cm.addKeyValue("commands", total_mqtt_client_commands_);
        cm.addKeyValue("responses", total_mqtt_server_responses_);

        for (auto &command: commands_) {
                const char *label = std::get<1>(command);

                cm.addKeyValue(label, std::get<2>(command));
        }

       	return cm;
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING)
#if defined(PYTHON_BINDING)
boost::python::dict MQTTProtocol::getCacheData(const std::string &name) const {
#elif defined(RUBY_BINDING)
VALUE MQTTProtocol::getCacheData(const std::string &name) const {
#endif
        if (boost::iequals(name, "topic"))
		return addMapToHash(topic_map_);

        return addMapToHash({});
}

#if defined(PYTHON_BINDING)
SharedPointer<Cache<StringCache>> MQTTProtocol::getCache(const std::string &name) {

        if (boost::iequals(name, "topic"))
                return topic_cache_;

        return nullptr;
}

#endif

#endif

void MQTTProtocol::resetCounters() {

	reset();

        total_events_ = 0;
        total_mqtt_client_commands_ = 0;
        total_mqtt_server_responses_ = 0;
        for (auto &command: commands_)
               std::get<2>(command) = 0;
}

} // namespace aiengine

