/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_FREQUENCY_FREQUENCYGROUP_H_
#define SRC_PROTOCOLS_FREQUENCY_FREQUENCYGROUP_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <utility>
#include <cstring>
#include "Frequencies.h"
#include "flow/FlowManager.h"
#include <boost/format.hpp>
#include "FrequencyGroupItem.h"

namespace aiengine {

template <class A_Type> 
class FrequencyGroup {
public:

	typedef std::shared_ptr< FrequencyGroup<A_Type> > Ptr;
	typedef std::weak_ptr< FrequencyGroup<A_Type>> PtrWeak;

    	explicit FrequencyGroup():
		name_(""),
		log_level_(0),
		total_process_flows_(0),
		total_computed_freqs_(0),
		group_map_(), 
		flow_list_() {
#if defined(RUBY_BINDING)
		flow_list_ = rb_ary_new();
#endif
	}

    	virtual ~FrequencyGroup() {}

	const char* getName() { return name_.c_str();} 
	void setName(char *name) { name_ = name;}

	void agregateFlows(const SharedPointer<FlowManager> &fm, std::function <A_Type (SharedPointer<Flow>&)> condition);
	void compute();
	void reset();

	void statistics(std::basic_ostream<char>& out) const { 
	
		out << "Frequency Group(" << name_ <<") total frequencies groups:" << group_map_.size() << "\n";
		out << "\tTotal process flows:" << total_process_flows_<< "\n";
		out << "\tTotal computed frequencies:" << total_computed_freqs_<< "\n";
		out << boost::format("\t%-22s %-10s %-10s %-10s %-10s") % "Key" % "Flows" % "Bytes" % "Dispersion" % "Enthropy";
		out << std::endl;
		for (auto it = group_map_.begin(); it != group_map_.end(); ++it) {
			FrequencyGroupItemPtr fgi = it->second;
	
			out << "\t";	
			out << boost::format("%-22s %-10d %-10d %-10d %-10d") % it->first % fgi->getTotalItems() % fgi->getTotalFlowsBytes() \
				% fgi->getFrequencies()->getDispersion() % fgi->getFrequencies()->getEntropy();
			out << "\n";	
			if (log_level_ > 0)
				out << "\t" << fgi->getFrequencies()->getFrequenciesString() << "\n";
		}
		out << std::endl;
	}	

	void statistics() const { statistics(std::cout); } 

	void setLogLevel(int level) { log_level_ = level;}

#if defined(RUBY_BINDING)
	void agregateAllFlows(const FlowManager &fm); 
	void agregateFlowsBySourcePort(const FlowManager &fm); 
	void agregateFlowsByDestinationPort(const FlowManager &fm); 
	void agregateFlowsBySourceAddress(const FlowManager &fm); 
	void agregateFlowsByDestinationAddress(const FlowManager &fm); 
	void agregateFlowsByDestinationAddressAndPort(const FlowManager &fm); 
	void agregateFlowsBySourceAddressAndPort(const FlowManager &fm); 
#else
	void agregateAllFlows(const SharedPointer<FlowManager> &fm);
	void agregateFlowsBySourcePort(const SharedPointer<FlowManager> &fm);
	void agregateFlowsByDestinationPort(const SharedPointer<FlowManager> &fm);
	void agregateFlowsBySourceAddress(const SharedPointer<FlowManager> &fm); 
	void agregateFlowsByDestinationAddress(const SharedPointer<FlowManager> &fm); 
	void agregateFlowsByDestinationAddressAndPort(const SharedPointer<FlowManager> &fm); 
	void agregateFlowsBySourceAddressAndPort(const SharedPointer<FlowManager> &fm); 
#endif
	int32_t getTotalProcessFlows() { return total_process_flows_;}
	int32_t getTotalComputedFrequencies() { return total_computed_freqs_;}

#if defined(PYTHON_BINDING)
	boost::python::list getReferenceFlows() { return flow_list_;};
	boost::python::list getReferenceFlowsByKey(A_Type key);
#elif defined(RUBY_BINDING)
	VALUE getReferenceFlows() { return flow_list_;};
	VALUE getReferenceFlowsByKey(A_Type key);
#else
	std::vector<WeakPointer<Flow>> &getReferenceFlows() { return flow_list_;};
	std::vector<WeakPointer<Flow>> &getReferenceFlowsByKey(A_Type key);
#endif
	typedef std::map <A_Type, FrequencyGroupItemPtr> GroupMapType;
	typedef typename GroupMapType::iterator iterator;
    	typedef typename GroupMapType::const_iterator const_iterator;

    	iterator begin() { return group_map_.begin();}
    	const_iterator begin() const {return group_map_.begin();}
    	const iterator cbegin() const {return group_map_.cbegin();}
    	iterator end() {return group_map_.end();}
    	const_iterator end() const {return group_map_.end();}
    	const iterator cend() const {return group_map_.cend();}

private:
	std::string name_;
	int log_level_;
	int32_t total_process_flows_;
	int32_t total_computed_freqs_;
	GroupMapType group_map_;
#if defined(PYTHON_BINDING)
	boost::python::list flow_list_;
#elif defined(RUBY_BINDING)
	VALUE flow_list_;
#else
	std::vector<WeakPointer<Flow>> flow_list_;
#endif
};

} // namespace aiengine

#include "FrequencyGroup_Impl.h"

#endif  // SRC_PROTOCOLS_FREQUENCY_FREQUENCYGROUP_H_
