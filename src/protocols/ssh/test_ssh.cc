/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_ssh.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE sshtest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(ssh_test_suite, StackSSHtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

	BOOST_CHECK(ssh->getTotalBytes() == 0);
	BOOST_CHECK(ssh->getTotalPackets() == 0);
	BOOST_CHECK(ssh->getTotalValidPackets() == 0);
	BOOST_CHECK(ssh->getTotalInvalidPackets() == 0);
	BOOST_CHECK(ssh->processPacket(packet) == true);
	
	CounterMap c = ssh->getCounters();

	BOOST_CHECK(ssh->isDynamicAllocatedMemory() == false);

	auto v1 = ssh->getCurrentUseMemory();
	auto v2 = ssh->getAllocatedMemory();
	auto v3 = ssh->getTotalAllocatedMemory();

	BOOST_CHECK(ssh->getTotalCacheMisses() == 0);

	ssh->decreaseAllocatedMemory(10);
}

BOOST_AUTO_TEST_CASE (test02)
{
	Packet packet1("../ssh/packets/packet02.pcap"); // Server packet
	Packet packet2("../ssh/packets/packet01.pcap"); // Client packet

	inject(packet1);

        Flow *flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
	BOOST_CHECK(flow->total_packets_l7 == 1);

	BOOST_CHECK(ssh->getTotalBytes() == 21);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalValidPackets() == 1);
	BOOST_CHECK(ssh->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->client_name == nullptr);
	BOOST_CHECK(info->server_name != nullptr);

	std::string sname("SSH-2.0-OpenSSH_5.3");
        BOOST_CHECK(sname.compare(info->server_name->getName()) == 0);

	inject(packet2);

        flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

	BOOST_CHECK(flow->total_packets_l7 == 2);
	std::string cname("SSH-2.0-paramiko_1.16.0");
        BOOST_CHECK(cname.compare(info->client_name->getName()) == 0);

	BOOST_CHECK(ssh->getTotalBytes() == 25 + 21);
	BOOST_CHECK(ssh->getTotalPackets() == 2);
	BOOST_CHECK(ssh->getTotalValidPackets() == 1);
	BOOST_CHECK(ssh->getTotalInvalidPackets() == 0);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 0);

        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 0);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);

        nlohmann::json j1;
        j1 << *info;

	// Force a release
	ssh->releaseFlowInfo(flow);
}

BOOST_AUTO_TEST_CASE (test03)
{
	Packet packet("../ssh/packets/packet03.pcap", 66);
        auto flow = SharedPointer<Flow>(new Flow());

	flow->total_packets_l7 = 3;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 600);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
        
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        
	Json j;

	ssh->statistics(j, 5);
	ssh->statistics(j, "names");
}

BOOST_AUTO_TEST_CASE (test04)
{
	Packet packet("../ssh/packets/packet04.pcap", 66);
        auto flow = SharedPointer<Flow>(new Flow());

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 784);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test05)
{
	Packet packet("../ssh/packets/packet05.pcap", 66);
        auto flow = SharedPointer<Flow>(new Flow());

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 144);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 0);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 1);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test06)
{
        auto flow = SharedPointer<Flow>(new Flow());
	Packet packet("../ssh/packets/packet06.pcap", 66);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 720);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 2);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 1);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);

	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	// The next packet from the server will be encrypted
	BOOST_CHECK(info->isServerHandshake() == false);
}

BOOST_AUTO_TEST_CASE (test07)
{
        auto flow = SharedPointer<Flow>(new Flow());
	Packet packet("../ssh/packets/packet07.pcap", 66);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 16);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	// The client will send the next packet encrypted
	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
	
	BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	BOOST_CHECK(ssh->getTotalEncryptedPackets() == 0);
}

BOOST_AUTO_TEST_CASE (test08) 
{
	Packet packet1("../ssh/packets/packet06.pcap", 66);
	Packet packet2("../ssh/packets/packet07.pcap", 66);
	Packet packet3("../ssh/packets/packet08.pcap", 66);

        auto flow = SharedPointer<Flow>(new Flow());

	ssh->setDynamicAllocatedMemory(true);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet1);

	// Inject the server packet
        ssh->processFlow(flow.get());

	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(info->isClientHandshake() == true);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
        
	flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet2);
       
	// Inject the client packet 
	ssh->processFlow(flow.get());

	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == false);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

	flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet3);

	// Inject the client encrypted packet        
	ssh->processFlow(flow.get());

	// The number of encrypted bytes should be non zero

	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == false);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 48);
	
	BOOST_CHECK(ssh->getTotalEncryptedBytes() == 48);
	BOOST_CHECK(ssh->getTotalEncryptedPackets() == 1);

	{
		RedirectOutput r;

        	flow->show(r.cout);
        	r.cout << *(info.get());
		ssh->statistics(r.cout, 5);
     	} 
}

BOOST_AUTO_TEST_CASE (test09) // Two pdus and the latest is encrypted
{
        auto flow = SharedPointer<Flow>(new Flow());
        Packet packet("../ssh/packets/packet12.pcap", 54);

        flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

        BOOST_CHECK(ssh->getTotalBytes() == 64);
        BOOST_CHECK(ssh->getTotalPackets() == 1);
        BOOST_CHECK(ssh->getTotalHandshakePDUs() == 2);

        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);
}

BOOST_AUTO_TEST_CASE (test10)
{
        Packet packet1("../ssh/packets/packet13.pcap"); // Server packet
        Packet packet2("../ssh/packets/packet14.pcap"); // Client packet

        inject(packet1);

        Flow *flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

        BOOST_CHECK(info->isHandshake() == true);
        BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
        BOOST_CHECK(flow->total_packets_l7 == 1);

        BOOST_CHECK(ssh->getTotalBytes() == 39);
        BOOST_CHECK(ssh->getTotalPackets() == 1);
        BOOST_CHECK(ssh->getTotalValidPackets() == 1);
        BOOST_CHECK(ssh->getTotalInvalidPackets() == 0);

        BOOST_CHECK(info->client_name == nullptr);
        BOOST_CHECK(info->server_name != nullptr);

        std::string sname("SSH-2.0-OpenSSH_5.3p1 Debian-3ubuntu3");
        BOOST_CHECK(sname.compare(info->server_name->getName()) == 0);

        inject(packet2);

        flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

        BOOST_CHECK(info->isHandshake() == true);
        BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

        BOOST_CHECK(flow->total_packets_l7 == 2);
        std::string cname("SSH-2.0-SharpSSH-1.1.1.13-JSCH-0.1.2");
        BOOST_CHECK(cname.compare(info->client_name->getName()) == 0);

        BOOST_CHECK(ssh->getTotalBytes() == 39 + 38);
        BOOST_CHECK(ssh->getTotalPackets() == 2);
        BOOST_CHECK(ssh->getTotalValidPackets() == 1);
        BOOST_CHECK(ssh->getTotalInvalidPackets() == 0);
        BOOST_CHECK(ssh->getTotalHandshakePDUs() == 0);

        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 0);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);

	Json j;

	ssh->statistics(j, 5);
	ssh->statistics(j, "names");

        // Force a release
        ssh->releaseFlowInfo(flow);
}

// Two different flows shares the same banner messages
BOOST_AUTO_TEST_CASE (test11)
{
        Packet packet1("../ssh/packets/packet13.pcap", 54); // Server packet
        Packet packet2("../ssh/packets/packet14.pcap", 54); // Client packet

        auto flow1 = SharedPointer<Flow>(new Flow());
        auto flow2 = SharedPointer<Flow>(new Flow());

        flow1->total_packets_l7 = 0;
        flow1->setFlowDirection(FlowDirection::FORWARD);
        flow1->packet = const_cast<Packet*>(&packet1);

        flow2->total_packets_l7 = 0;
        flow2->setFlowDirection(FlowDirection::FORWARD);
        flow2->packet = const_cast<Packet*>(&packet1);

        ssh->processFlow(flow1.get());
        ssh->processFlow(flow2.get());

        SharedPointer<SSHInfo> info1 = flow1->getSSHInfo();
        SharedPointer<SSHInfo> info2 = flow2->getSSHInfo();
        BOOST_CHECK(info1 != nullptr);
        BOOST_CHECK(info2 != nullptr);

        BOOST_CHECK(info1->client_name == nullptr);
        BOOST_CHECK(info2->client_name == nullptr);
        BOOST_CHECK(info1->server_name != nullptr);
        BOOST_CHECK(info2->server_name != nullptr);
        BOOST_CHECK(info1->server_name == info2->server_name);

        std::string sname("SSH-2.0-OpenSSH_5.3p1 Debian-3ubuntu3");
        BOOST_CHECK(sname.compare(info1->server_name->getName()) == 0);

	// Now inject the other packet
        flow1->setFlowDirection(FlowDirection::BACKWARD);
        flow1->packet = const_cast<Packet*>(&packet2);

        flow2->setFlowDirection(FlowDirection::BACKWARD);
        flow2->packet = const_cast<Packet*>(&packet2);
        
	ssh->processFlow(flow1.get());
        ssh->processFlow(flow2.get());

        BOOST_CHECK(info1->client_name != nullptr);
        BOOST_CHECK(info2->client_name != nullptr);
        BOOST_CHECK(info1->client_name == info2->client_name);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(ipv6_ssh_test_suite, StackIPv6SSHtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet("../ssh/packets/packet11.pcap");

        inject(packet);

        Flow *flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

        BOOST_CHECK(info->isHandshake() == true);
        BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

        BOOST_CHECK(ssh->getTotalBytes() == 19);
        BOOST_CHECK(ssh->getTotalPackets() == 1);
        BOOST_CHECK(ssh->getTotalValidPackets() == 1);
        BOOST_CHECK(ssh->getTotalInvalidPackets() == 0);

	{
		RedirectOutput r;

        	flow->show(r.cout);
        	r.cout << *(info.get());
        	ssh->statistics(r.cout, 5);
	}

	ssh->releaseCache();
}

BOOST_AUTO_TEST_CASE (test02)
{
	Packet packet("../ssh/packets/packet09.pcap", 74);
        auto flow = SharedPointer<Flow>(new Flow());

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 16);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);
	
	BOOST_CHECK(info->isClientHandshake() == true);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test03)
{
        auto flow = SharedPointer<Flow>(new Flow());
	Packet packet("../ssh/packets/packet10.pcap", 74);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 16);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);
	
	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->isServerHandshake() == true);
	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test04) // Memory failure test
{
	Packet packet("../ssh/packets/packet11.pcap");

	ssh->decreaseAllocatedMemory(10);

        inject(packet);

        Flow *flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info == nullptr);
}

BOOST_AUTO_TEST_CASE (test05) // Bogus length on the packet
{
        auto flow = SharedPointer<Flow>(new Flow());
	Packet packet("../ssh/packets/packet10.pcap", 74);
	uint8_t buffer[128];

	std::memcpy(buffer, packet.getPayload(), 128);
	buffer[1] = '\xff';
	buffer[2] = '\xff';
	buffer[3] = '\xff';
        Packet packet_mod(buffer, packet.getLength() - 74);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet_mod);

        ssh->processFlow(flow.get());

        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);
	
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 0);
}

BOOST_AUTO_TEST_SUITE_END()
