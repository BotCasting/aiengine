/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef _TEST_PPPOE_H_
#define _TEST_PPPOE_H_

#include <string>
#include <cstring>
#include "Protocol.h"
#include "StackTest.h"
#include "protocols/ip/IPProtocol.h"
#include "protocols/ip6/IPv6Protocol.h"
#include "PPPoEProtocol.h"

using namespace aiengine;

struct StackTestPPPoE : public StackTest
{
        PPPoEProtocolPtr pppoe;
        MultiplexerPtr mux_pppoe;
        IPProtocolPtr ip;
        IPv6ProtocolPtr ip6;
        MultiplexerPtr mux_ip;
        MultiplexerPtr mux_ip6;

        StackTestPPPoE()
        {
        	pppoe = PPPoEProtocolPtr(new PPPoEProtocol());
        	mux_pppoe = MultiplexerPtr(new Multiplexer());
       	
		ip = IPProtocolPtr(new IPProtocol());
		ip6 = IPv6ProtocolPtr(new IPv6Protocol());

        	mux_ip = MultiplexerPtr(new Multiplexer());
        	mux_ip6 = MultiplexerPtr(new Multiplexer());

        	// configure the pppoe handler
        	pppoe->setMultiplexer(mux_pppoe);
		mux_pppoe->setProtocol(static_cast<ProtocolPtr>(pppoe));

		// configure the ip handler
		ip->setMultiplexer(mux_ip);
		mux_ip->setProtocol(static_cast<ProtocolPtr>(ip));

		// configure the ipv6 handler
		ip6->setMultiplexer(mux_ip6);
		mux_ip6->setProtocol(static_cast<ProtocolPtr>(ip6));

		// configure the multiplexers
		mux_eth->addUpMultiplexer(mux_pppoe);
		mux_pppoe->addDownMultiplexer(mux_eth);

                // configure the multiplexers of the first part
                mux_pppoe->addUpMultiplexer(mux_ip);
                mux_ip->addDownMultiplexer(mux_pppoe);

                mux_pppoe->addUpMultiplexer(mux_ip6);
                mux_ip6->addDownMultiplexer(mux_pppoe);
	}

        ~StackTestPPPoE() {}
};

#endif
