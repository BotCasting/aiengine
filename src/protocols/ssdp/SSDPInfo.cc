/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "SSDPInfo.h"

namespace aiengine {

void SSDPInfo::reset() {

	is_banned_ = false; 
	total_requests_ = 0;
	total_responses_ = 0;
	response_code_ = 0;
	host_name.reset(); 
	uri.reset();
	matched_domain_name.reset();
}

void SSDPInfo::resetStrings() { 

	uri.reset();
	host_name.reset(); 
}

std::ostream& operator<< (std::ostream &out, const SSDPInfo &info) {

	if (info.isBanned()) 
		out << " Banned";

	if (info.host_name) 
		out << " Host:" << info.host_name->getName();

	if (info.uri) 
		out << " Uri:" << info.uri->getName();

	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const SSDPInfo &info) {

	out["requests"] = info.total_requests_;
	out["responses"] = info.total_responses_;

	if (info.isBanned()) 
		out["banned"] = true;

	if (info.host_name) 
		out["host"] = info.host_name->getName();

	if (info.uri) 
		out["uri"] = info.uri->getName();

        if (info.matched_domain_name) 
		out["matchs"] = info.matched_domain_name->getName();

	return out;
}

} // namespace aiengine

