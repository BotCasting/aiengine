/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "DHCPv6Info.h"

namespace aiengine {

void DHCPv6Info::reset() { 
	t1_ = 0;
	t2_ = 0;
	host_name.reset();
	ip6.reset();
}

std::ostream& operator<< (std::ostream &out, const DHCPv6Info &info) {

	if (info.host_name) 
		out << " Host:" << info.host_name->getName();

	if (info.ip6) 
		out << " IPv6:" << info.ip6->getName();

	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const DHCPv6Info &info) {

	if (info.host_name) 
		out["host"] = info.host_name->getName();

	if (info.ip6) 
		out["ipv6"] = info.ip6->getName();

        if ((info.t1_ > 0)and(info.t2_ > 0)) {
                out["t1"] = info.t1_; 
                out["t2"] = info.t2_; 
	}

	return out;
}

} // namespace aiengine

