/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "DHCPv6Protocol.h"
#include <iomanip>

namespace aiengine {

DHCPv6Protocol::DHCPv6Protocol():
	Protocol("DHCPv6", IPPROTO_UDP) {}

bool DHCPv6Protocol::check(const Packet &packet) {

	int length = packet.getLength();

	if (length >= header_size) {
		if ((packet.getDestinationPort() == 547)||(packet.getDestinationPort() == 546)) {
			setHeader(packet.getPayload());
			++total_valid_packets_;
			return true;
		}
	}
	++total_invalid_packets_;
	return false;
}

void DHCPv6Protocol::setDynamicAllocatedMemory(bool value) {

	info_cache_->setDynamicAllocatedMemory(value);
	host_cache_->setDynamicAllocatedMemory(value);
	ip6_cache_->setDynamicAllocatedMemory(value);
}

bool DHCPv6Protocol::isDynamicAllocatedMemory() const {

	return info_cache_->isDynamicAllocatedMemory();
}

int64_t DHCPv6Protocol::getCurrentUseMemory() const {

	int64_t mem = sizeof(DHCPv6Protocol);

	mem += info_cache_->getCurrentUseMemory();
	mem += host_cache_->getCurrentUseMemory();
	mem += ip6_cache_->getCurrentUseMemory();

	return mem;
}

int64_t DHCPv6Protocol::getAllocatedMemory() const {

        int64_t mem = sizeof(DHCPv6Protocol);

        mem += info_cache_->getAllocatedMemory();
        mem += host_cache_->getAllocatedMemory();
        mem += ip6_cache_->getAllocatedMemory();

        return mem;
}

int64_t DHCPv6Protocol::getTotalAllocatedMemory() const {

        return getAllocatedMemory();
}

int64_t DHCPv6Protocol::compute_memory_used_by_maps() const {

	int64_t bytes = 0;

	std::for_each (host_map_.begin(), host_map_.end(), [&bytes] (PairStringCacheHits const &f) {
		bytes += f.first.size();
	});
	std::for_each (ip6_map_.begin(), ip6_map_.end(), [&bytes] (PairStringCacheHits const &f) {
		bytes += f.first.size();
	});

	return bytes;
}

int32_t DHCPv6Protocol::getTotalCacheMisses() const {

	int32_t miss = 0;

	miss = info_cache_->getTotalFails();
	miss += host_cache_->getTotalFails();
	miss += ip6_cache_->getTotalFails();

	return miss;
}

void DHCPv6Protocol::releaseCache() {

        if (FlowManagerPtr fm = flow_mng_.lock(); fm) {
                auto ft = fm->getFlowTable();

                std::ostringstream msg;
                msg << "Releasing " << getName() << " cache";

                infoMessage(msg.str());

                int64_t total_cache_bytes_released = compute_memory_used_by_maps();
                int64_t total_bytes_released_by_flows = 0;
                int64_t total_cache_save_bytes = 0;
                int32_t release_flows = 0;
                int32_t release_host = host_map_.size();
                int32_t release_ips = ip6_map_.size();

                for (auto &flow: ft) {
                        if (SharedPointer<DHCPv6Info> info = flow->getDHCPv6Info(); info) {
                                total_bytes_released_by_flows += sizeof(info);

                                flow->layer7info.reset();
                                ++release_flows;
                                info_cache_->release(info);
                        }
                }
                // Some entries can be still on the maps and needs to be
                // retrieve to their existing caches
                for (auto &entry: host_map_) {
			total_cache_save_bytes += entry.second.sc->getNameSize() * (entry.second.hits - 1);
                        releaseStringToCache(host_cache_, entry.second.sc);
		}
                host_map_.clear();

                for (auto &entry: ip6_map_) {
			total_cache_save_bytes += entry.second.sc->getNameSize() * (entry.second.hits - 1);
                        releaseStringToCache(ip6_cache_, entry.second.sc);
		}
                ip6_map_.clear();

                msg.str("");
                msg << "Release " << release_host << " host names, " << release_ips << " ips, "  << release_flows << " flows";
		computeMemoryUtilization(msg, total_cache_bytes_released, total_bytes_released_by_flows, total_cache_save_bytes);
                infoMessage(msg.str());
        }
}

void DHCPv6Protocol::releaseFlowInfo(Flow *flow) {

	if (auto info = flow->getDHCPv6Info(); info)
		info_cache_->release(info);
}

void DHCPv6Protocol::attach_host_name(DHCPv6Info *info, const boost::string_ref &name) {

        if (!info->host_name) {
                if (auto it = host_map_.find(name); it != host_map_.end()) {
                        ++(it->second).hits;
                        info->host_name = (it->second).sc;
		} else {
                        if (auto host_ptr = host_cache_->acquire(); host_ptr) {
                                host_ptr->setName(name.data(), name.length());
                                info->host_name = host_ptr;
                                host_map_.insert(std::make_pair(host_ptr->getName(), host_ptr));
                        }
                }
        }
}

void DHCPv6Protocol::attach_ip(DHCPv6Info *info, const boost::string_ref &ip) {

        if (!info->ip6) {
                if (auto it = ip6_map_.find(ip); it != ip6_map_.end()) {
                        ++(it->second).hits;
                        info->ip6 = (it->second).sc;
                } else {
                        if (auto ip_ptr = ip6_cache_->acquire(); ip_ptr) {
                                ip_ptr->setName(ip.data(), ip.length());
                                info->ip6 = ip_ptr;
                                ip6_map_.insert(std::make_pair(ip_ptr->getName(), ip_ptr));
                        }
                }
        }
}

void DHCPv6Protocol::handle_request(DHCPv6Info *info, const uint8_t *payload, int length) {

        int idx = 0;
        while (idx < length) {
		const dhcpv6_option *opt = reinterpret_cast<const dhcpv6_option*>(&payload[idx]);
        	uint16_t code = ntohs(opt->code);
                uint16_t len = ntohs(opt->len);

		if (idx + len < length) {
			//std::cout << "idx:" << idx << " code:" << code << " len:" << len << std::endl;
                	if (code == 39) { // Fully domain
				boost::string_ref name(reinterpret_cast<const char*>(&(opt->data[0]) + 2), len - 2);

                        	attach_host_name(info, name);
                        	break;
			} else if (code == 3) { // DHCPv6 identity association for non-temporary address
				const dhcpv6_ia_na_option *ia_hdr = reinterpret_cast<const dhcpv6_ia_na_option*>(&(opt->data[0]));
				uint32_t renew = ntohl(ia_hdr->renew);
				uint32_t rebind = ntohl(ia_hdr->rebind);

				if ((renew > 0)and(rebind > 0)) {
					info->setLifetime(renew, rebind);
				}

				const dhcpv6_option *opt = reinterpret_cast<const dhcpv6_option*>(&ia_hdr->options[0]);
				if (ntohs(opt->code) == 5) {
					const dhcpv6_iaaddr_option *addr_hdr = reinterpret_cast<const dhcpv6_iaaddr_option*>(&opt->data[0]);
					char address6[INET6_ADDRSTRLEN];

					inet_ntop(AF_INET6, &(addr_hdr->address), address6, INET6_ADDRSTRLEN);

					boost::string_ref addr_ref(address6);

					attach_ip(info, addr_ref);
				}
			}
		}
                idx += sizeof(dhcpv6_option) + (int)len;
	}
}

void DHCPv6Protocol::processFlow(Flow *flow) {

	int length = flow->packet->getLength();
	total_bytes_ += length;

	current_flow_ = flow;

	++total_packets_;

        if (length >= header_size) {
                SharedPointer<DHCPv6Info> info = flow->getDHCPv6Info();
                if (!info) {
                        if (info = info_cache_->acquire(); !info) {
				logFailCache(info_cache_->getName(), flow);
                                return;
                        }
                        flow->layer7info = info;
                }
		setHeader(flow->packet->getPayload());

		uint8_t type = getType();

		if (type == DHCPV6_SOLICIT) {
			handle_request(info.get(), (uint8_t*)&header_->options, length - header_size);
			++total_dhcpv6_solicit_;
		} else if (type == DHCPV6_ADVERTISE) {
			++total_dhcpv6_advertise_;
		} else if (type == DHCPV6_REQUEST) {
			++total_dhcpv6_request_;
		} else if (type == DHCPV6_CONFIRM) {
			++total_dhcpv6_confirm_;
		} else if (type == DHCPV6_RENEW) {
			handle_request(info.get(), (uint8_t*)&header_->options, length - header_size);
			++total_dhcpv6_renew_;
		} else if (type == DHCPV6_REBIND) {
			++total_dhcpv6_rebind_;
		} else if (type == DHCPV6_REPLY) {
			handle_request(info.get(), (uint8_t*)&header_->options, length - header_size);
			++total_dhcpv6_reply_;
		} else if (type == DHCPV6_RELEASE) {
			++total_dhcpv6_release_;
		} else if (type == DHCPV6_DECLINE) {
			++total_dhcpv6_decline_;
		} else if (type == DHCPV6_RECONFIGURE) {
			++total_dhcpv6_reconfigure_;
		} else if (type == DHCPV6_INFO_REQUEST) {
			++total_dhcpv6_info_request_;
		} else if (type == DHCPV6_RELAY_FORW) {
			++total_dhcpv6_relay_forw_;
		} else if (type == DHCPV6_RELAY_REPL) {
			++total_dhcpv6_relay_repl_;
		}
	}
}

void DHCPv6Protocol::statistics(std::basic_ostream<char> &out, int level) const {

	showStatisticsHeader(out, level);

	if (level > 3) {
		out << "\t" << "Total solicits:         " << std::setw(10) << total_dhcpv6_solicit_ << "\n";
		out << "\t" << "Total advertises:       " << std::setw(10) << total_dhcpv6_advertise_ << "\n";
		out << "\t" << "Total requests:         " << std::setw(10) << total_dhcpv6_request_ << "\n";
		out << "\t" << "Total confirms:         " << std::setw(10) << total_dhcpv6_confirm_ << "\n";
		out << "\t" << "Total renews:           " << std::setw(10) << total_dhcpv6_renew_ << "\n";
		out << "\t" << "Total rebinds:          " << std::setw(10) << total_dhcpv6_rebind_ << "\n";
		out << "\t" << "Total replys:           " << std::setw(10) << total_dhcpv6_reply_ << "\n";
		out << "\t" << "Total releases:         " << std::setw(10) << total_dhcpv6_release_ << "\n";
		out << "\t" << "Total declines:         " << std::setw(10) << total_dhcpv6_decline_ << "\n";
		out << "\t" << "Total reconfigures:     " << std::setw(10) << total_dhcpv6_reconfigure_ << "\n";
		out << "\t" << "Total info requests:    " << std::setw(10) << total_dhcpv6_info_request_ << "\n";
		out << "\t" << "Total relay forws:      " << std::setw(10) << total_dhcpv6_relay_forw_ << "\n";
		out << "\t" << "Total relay repls:      " << std::setw(10) << total_dhcpv6_relay_repl_ << std::endl;
	}
	if (level > 5)
		if (flow_forwarder_.lock())
			flow_forwarder_.lock()->statistics(out);
	if (level > 3) {
		info_cache_->statistics(out);
		host_cache_->statistics(out);
		ip6_cache_->statistics(out);
		if (level > 4) {
			showCacheMap(out, "\t", host_map_, "Host names", "Host");
			showCacheMap(out, "\t", ip6_map_, "IPv6 Addresses", "IPv6");
		}
	}
}

void DHCPv6Protocol::statistics(Json &out, int level) const {

	showStatisticsHeader(out, level);

        if (level > 3) {
		out["solicits"] = total_dhcpv6_solicit_;
		out["advertises"] = total_dhcpv6_advertise_;
		out["requests"] = total_dhcpv6_request_;
		out["confirms"] = total_dhcpv6_confirm_;
		out["renews"] = total_dhcpv6_renew_;
		out["rebinds"] = total_dhcpv6_rebind_;
		out["replys"] = total_dhcpv6_reply_;
		out["releases"] = total_dhcpv6_release_;
		out["declines"] = total_dhcpv6_decline_;
		out["reconfigures"] = total_dhcpv6_reconfigure_;
		out["info_requests"] = total_dhcpv6_info_request_;
		out["relay_forws"] = total_dhcpv6_relay_forw_;
		out["relay_repls"] = total_dhcpv6_relay_repl_;
        }
}

void DHCPv6Protocol::increaseAllocatedMemory(int value) {

        info_cache_->create(value);
        host_cache_->create(value);
        ip6_cache_->create(value);
}

void DHCPv6Protocol::decreaseAllocatedMemory(int value) {

        info_cache_->destroy(value);
        host_cache_->destroy(value);
        ip6_cache_->destroy(value);
}

CounterMap DHCPv6Protocol::getCounters() const {
	CounterMap cm;

        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);

        cm.addKeyValue("solicits", total_dhcpv6_solicit_);
        cm.addKeyValue("advertises", total_dhcpv6_advertise_);
        cm.addKeyValue("requests", total_dhcpv6_request_);
        cm.addKeyValue("confirms", total_dhcpv6_confirm_);
        cm.addKeyValue("renews", total_dhcpv6_renew_);
        cm.addKeyValue("rebinds", total_dhcpv6_rebind_);
        cm.addKeyValue("replys", total_dhcpv6_reply_);
        cm.addKeyValue("releases", total_dhcpv6_release_);
        cm.addKeyValue("declines", total_dhcpv6_decline_);
        cm.addKeyValue("reconfigures", total_dhcpv6_reconfigure_);
        cm.addKeyValue("info requests", total_dhcpv6_info_request_);
        cm.addKeyValue("relay forws", total_dhcpv6_relay_forw_);
        cm.addKeyValue("relay repls", total_dhcpv6_relay_repl_);

        return cm;
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING)
#if defined(PYTHON_BINDING)
boost::python::dict DHCPv6Protocol::getCacheData(const std::string &name) const {
#elif defined(RUBY_BINDING)
VALUE DHCPv6Protocol::getCacheData(const std::string &name) const {
#endif
        if (boost::iequals(name, "name")or(boost::iequals(name, "host")))
		return addMapToHash(host_map_);
        else if (boost::iequals(name, "ip6"))
		return addMapToHash(ip6_map_);
        else if (boost::iequals(name, "ip"))
		return addMapToHash(ip6_map_);

        return addMapToHash({});
}

#if defined(PYTHON_BINDING)
SharedPointer<Cache<StringCache>> DHCPv6Protocol::getCache(const std::string &name) {

        if (boost::iequals(name, "name")or(boost::iequals(name, "host")))
                return host_cache_;
        else if (boost::iequals(name, "ip6"))
                return ip6_cache_;
        else if (boost::iequals(name, "ip"))
                return ip6_cache_;

        return nullptr;
}

#endif

#endif

void DHCPv6Protocol::resetCounters() {

	reset();

        total_dhcpv6_solicit_ = 0;
        total_dhcpv6_advertise_ = 0;
        total_dhcpv6_request_ = 0;
        total_dhcpv6_confirm_ = 0;
        total_dhcpv6_renew_ = 0;
        total_dhcpv6_rebind_ = 0;
        total_dhcpv6_reply_ = 0;
        total_dhcpv6_release_ = 0;
        total_dhcpv6_decline_ = 0;
        total_dhcpv6_reconfigure_ = 0;
        total_dhcpv6_info_request_ = 0;
        total_dhcpv6_relay_forw_ = 0;
        total_dhcpv6_relay_repl_ = 0;
}

} // namespace aiengine
