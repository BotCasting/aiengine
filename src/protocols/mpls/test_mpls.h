/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef _TEST_MPLS_H_
#define _TEST_MPLS_H_

#include <string>
#include "Protocol.h"
#include "StackTest.h"
#include "flow/FlowCache.h"
#include "flow/FlowManager.h"
#include "protocols/ip/IPProtocol.h"
#include "protocols/icmp/ICMPProtocol.h"
#include "MPLSProtocol.h"

using namespace aiengine;

struct StackMPLStest : public StackTest
{
        MPLSProtocolPtr mpls;
        IPProtocolPtr ip;
	ICMPProtocolPtr icmp;
        MultiplexerPtr mux_mpls;
        MultiplexerPtr mux_ip;
        MultiplexerPtr mux_icmp;

        StackMPLStest()
        {
                ip = IPProtocolPtr(new IPProtocol());
		mpls = MPLSProtocolPtr(new MPLSProtocol());
		icmp = ICMPProtocolPtr(new ICMPProtocol());

                mux_ip = MultiplexerPtr(new Multiplexer());
                mux_mpls = MultiplexerPtr(new Multiplexer());
		mux_icmp = MultiplexerPtr(new Multiplexer());

                // configure the mpls handler
                mpls->setMultiplexer(mux_mpls);
                mux_mpls->setProtocol(static_cast<ProtocolPtr>(mpls));

                // configure the ip handler
                ip->setMultiplexer(mux_ip);
                mux_ip->setProtocol(static_cast<ProtocolPtr>(ip));

                //configure the icmp
                icmp->setMultiplexer(mux_icmp);
                mux_icmp->setProtocol(static_cast<ProtocolPtr>(icmp));

                // configure the multiplexers of the first part
                mux_eth->addUpMultiplexer(mux_mpls);
		mux_mpls->addDownMultiplexer(mux_eth);
		mux_mpls->addUpMultiplexer(mux_ip);
                mux_ip->addDownMultiplexer(mux_mpls);
                mux_ip->addUpMultiplexer(mux_icmp);
		mux_icmp->addDownMultiplexer(mux_ip);
        }

        ~StackMPLStest() {}
};

#endif
