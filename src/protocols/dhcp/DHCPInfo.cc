/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "DHCPInfo.h"

namespace aiengine {

void DHCPInfo::reset() { 
	lease_time_ = 0;
	host_name.reset();
	ip.reset();
}

std::ostream& operator<< (std::ostream &out, const DHCPInfo &info) {

	if (info.host_name) 
		out << " Host:" << info.host_name->getName();

	if (info.ip) 
		out << " IP:" << info.ip->getName();

	if (info.getLeaseTime() > 0) 
		out << " Lease:" << info.getLeaseTime();

	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const DHCPInfo &info) {

	if (info.host_name) 
		out["host"] = info.host_name->getName();

	if (info.ip) 
		out["ip"] = info.ip->getName();

	if (info.getLeaseTime() > 0) 
		out["lease"] = info.getLeaseTime();

	return out;
}

} // namespace aiengine

