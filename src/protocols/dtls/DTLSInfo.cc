/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "DTLSInfo.h"

namespace aiengine {

void DTLSInfo::reset() {

	encrypted_ = false;
	data_pdus_ = 0;
	version_ = 0;
}
 
std::ostream& operator<< (std::ostream &out, const DTLSInfo &info) {

	out << " Pdus:" << info.getTotalDataPdus();
	out << " Ver:0x" << std::hex << info.version_ << std::dec;
	
	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const DTLSInfo &info) {

        out["pdus"] = info.getTotalDataPdus();
        out["version"] = info.version_;

        return out;
}

} // namespace aiengine  
