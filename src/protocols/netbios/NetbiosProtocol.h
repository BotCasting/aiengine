/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_NETBIOS_NETBIOSPROTOCOL_H_
#define SRC_PROTOCOLS_NETBIOS_NETBIOSPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include "NetbiosInfo.h"
#include "Cache.h"
#include <arpa/inet.h>
#include "flow/FlowManager.h"

namespace aiengine {

struct netbios_header {
	uint16_t 	id;
	uint16_t 	flags;
	uint16_t 	questions;
	uint16_t 	answers;
	uint16_t 	auths;
	uint16_t 	adds;
	uint8_t 	data[0];
} __attribute__((packed)); 

class NetbiosProtocol: public Protocol {
public:
    	explicit NetbiosProtocol();
    	virtual ~NetbiosProtocol();

	static constexpr int header_size = sizeof(netbios_header);

	uint16_t getId() const override { return 0x0000; }
	int getHeaderSize() const override { return header_size;}

	// Condition for say that a packet is netbios
	bool check(const Packet &packet) override;
        void processFlow(Flow *flow) override;
        bool processPacket(Packet &packet) override { return true; } 

	void statistics(std::basic_ostream<char> &out, int level) const override;
	void statistics(Json &out, int level) const override;
	void statistics(Json &out, const std::string &map_name) const override;

	void releaseCache() override; 

	void setHeader(const uint8_t *raw_packet) override { 

		header_ = reinterpret_cast <const netbios_header*> (raw_packet);
	}

        void increaseAllocatedMemory(int value) override; 
        void decreaseAllocatedMemory(int value) override; 

        void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const override;
        int64_t getAllocatedMemory() const override; 
        int64_t getTotalAllocatedMemory() const override; 

        void setDynamicAllocatedMemory(bool value) override;
        bool isDynamicAllocatedMemory() const override;

	int32_t getTotalCacheMisses() const override;
	int32_t getTotalEvents() const override { return total_events_; }

	CounterMap getCounters() const override; 
	void resetCounters() override;

#if defined(PYTHON_BINDING)
        boost::python::dict getCacheData(const std::string &name) const override;
        SharedPointer<Cache<StringCache>> getCache(const std::string &name) override;
#elif defined(RUBY_BINDING)
        VALUE getCacheData(const std::string &name) const;
#endif
        void setAnomalyManager(SharedPointer<AnomalyManager> amng) override { anomaly_ = amng; }

	void releaseFlowInfo(Flow *flow) override;

	Flow* getCurrentFlow() const { return current_flow_; }

private:
	void attach_netbios_name(NetbiosInfo *info, const boost::string_ref &name);
	int64_t compute_memory_used_by_maps() const;

	const netbios_header *header_ = nullptr;
	int32_t total_events_ = 0;

        Cache<NetbiosInfo>::CachePtr info_cache_ = Cache<NetbiosInfo>::CachePtr(new Cache<NetbiosInfo>("Netbios Info cache"));
        Cache<StringCache>::CachePtr name_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("Name cache"));

        GenericMapType name_map_ {};

	FlowManagerPtrWeak flow_mng_ = FlowManagerPtrWeak();
	Flow *current_flow_ = nullptr;
        SharedPointer<AnomalyManager> anomaly_  = nullptr;
	uint8_t netbios_name_[32] = { 0x00 };
};

typedef std::shared_ptr<NetbiosProtocol> NetbiosProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_NETBIOS_NETBIOSPROTOCOL_H_
