/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_QUIC_QUICINFO_H_
#define SRC_PROTOCOLS_QUIC_QUICINFO_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include "Pointer.h"
#include "StringCache.h"
#include "FlowInfo.h"
#include "names/DomainName.h"

namespace aiengine {

class QuicInfo : public FlowInfo {
public:
    	explicit QuicInfo() { reset(); }
    	virtual ~QuicInfo() {}

	void reset(); 

	SharedPointer<StringCache> server_name;
	SharedPointer<StringCache> user_agent;

	SharedPointer<DomainName> matched_domain_name;

	friend std::ostream& operator<< (std::ostream &out, const QuicInfo &info); 
	friend nlohmann::json& operator<< (nlohmann::json& out, const QuicInfo &info);

#if defined(BINDING)
	const char *getServerName() const { return (server_name ? server_name->getName() : ""); }
	const char *getUserAgent() const { return (user_agent ? user_agent->getName() : ""); }
#endif

#if defined(PYTHON_BINDING)
        SharedPointer<DomainName> getMatchedDomainName() const { return matched_domain_name;}
#elif defined(RUBY_BINDING)
        DomainName& getMatchedDomainName() const { return *matched_domain_name.get();}
#elif defined(JAVA_BINDING) || defined(LUA_BINDING)
        DomainName& getMatchedDomainName() const { return *matched_domain_name.get();}
#endif
};

} // namespace aiengine

#endif  // SRC_PROTOCOLS_QUIC_QUICINFO_H_
