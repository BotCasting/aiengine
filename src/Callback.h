/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#pragma once
#ifndef SRC_CALLBACK_H_
#define SRC_CALLBACK_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#if defined(PYTHON_BINDING)
#include "PyGilContext.h"
#include <boost/python.hpp>
#include <boost/function.hpp>
#elif defined(RUBY_BINDING)
#include <ruby.h>
#include <ruby/version.h>
#elif defined(JAVA_BINDING)
#include "JaiCallback.h"
#elif defined(LUA_BINDING)
#include <lua.hpp>
#elif defined(GO_BINDING)
#include "GoaiCallback.h"
#endif
#include "Color.h"
#include "Logger.h"

namespace aiengine {

class Flow;
// http://www.lysator.liu.se/~norling/ruby_callbacks.html
class Callback {
#if defined(PYTHON_BINDING)
public:
	Callback() {}
	virtual ~Callback() {}

	bool haveCallback() const { return callback_set_; }

	void setCallbackWithNoArgs(PyObject *callback);
	void setCallback(PyObject *callback);
	void executeCallback(Flow *flow);
	bool executeCallback();

	PyObject *getCallback() const { return callback_; }
	const char *getCallbackName() const;

private:
	bool callback_set_ = false;
	PyObject *callback_ = nullptr;
#elif defined(RUBY_BINDING)
public:
	Callback() {
		memory_wrapper_ = Data_Wrap_Struct(0 /* klass */, staticMark, NULL, static_cast<void*>(this));
		rb_gc_register_address(&memory_wrapper_);
	}
	virtual ~Callback() { rb_gc_unregister_address(&memory_wrapper_); }

	bool haveCallback() const { return callback_set_;}

	void setCallback(VALUE callback);
	void setCallbackWithNoArgs(VALUE callback);
	VALUE getCallback() const { return callback_; }
	const char *getCallbackName() const { return ""; } // TODO

	// verify that haveCallback is true before execute the function
	void executeCallback(Flow *flow);
	void executeCallback();

protected:
	static void staticMark(Callback *me) { me->mark(); }

	void mark();
private:
	bool callback_set_ = false;
	VALUE callback_ = Qnil;
	VALUE memory_wrapper_ = Qnil;
#elif defined(JAVA_BINDING)
public:
	Callback() {}
	virtual ~Callback() {}

	bool haveCallback() const { return callback_set_;}

	void setCallback(JaiCallback *callback);
	JaiCallback *getCallback() const { return callback_; }
	const char *getCallbackName() const { return ""; } // TODO

	void executeCallback(Flow *flow);

private:
	bool callback_set_ = false;
	JaiCallback *callback_ = nullptr;
#elif defined(LUA_BINDING)
public:
	Callback() {}
	virtual ~Callback();

	bool haveCallback() const { return callback_set_; }

	const char *getCallback() const { return callback_name_.c_str(); }
	const char *getCallbackName() const { return callback_name_.c_str(); }

	void setCallback(lua_State* L, const char *callback);
	void setCallbackWithNoArgs(lua_State* L, const char *callback);
	void executeCallback(Flow *flow);
	bool executeCallback();
private:
	bool push_pointer(lua_State *L, void* ptr, const char* type_name, int owned = 0);

	std::string callback_name_ = "";
	int ref_function_ = LUA_NOREF;
	bool callback_set_ = false;
	lua_State *L_ = nullptr;
#elif defined(GO_BINDING)
public:
        Callback() {}
        virtual ~Callback() {}

        bool haveCallback() const { return callback_set_;}

        void setCallback(GoaiCallback *callback);
        GoaiCallback *getCallback() const { return callback_; }
        const char *getCallbackName() const { return ""; } // TODO

        void executeCallback(Flow *flow);

private:
        bool callback_set_ = false;
        GoaiCallback *callback_ = nullptr;
#else
public:
	Callback() {}
	virtual ~Callback() {}

	bool haveCallback() const { return callback_set_; }
private:
	bool callback_set_ = false;
#endif
};

} // namespace aiengine

#endif  // SRC_CALLBACK_H_

