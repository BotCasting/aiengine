/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_MESSAGE_H_
#define SRC_MESSAGE_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <fstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <chrono>
#include "Color.h"

namespace aiengine {

static std::function <void(const std::string&, const Color::Modifier&)> 
	generic_message = [] (const std::string &msg, const Color::Modifier &color) {

        boost::posix_time::ptime t(boost::posix_time::microsec_clock::local_time());
        std::ostringstream out;

        out << t.date().day() << "/" << t.date().month() << "/" << t.date().year() << " ";
        out << t.time_of_day().hours() << ":" << t.time_of_day().minutes() << ":" << t.time_of_day().seconds();

        if (isatty(fileno(stdout))) {
                Color::Modifier green(Color::FG_GREEN);
                Color::Modifier def(Color::FG_DEFAULT);
                std::cout << green << "[" << out.str() << "] " << color << msg << def << std::endl;
        } else {
                std::cout << "[" << out.str() << "] " << msg << std::endl;
        }
};

// Generic function for pretty print information messages
static std::function <void(const std::string&)> information_message = [] (const std::string &msg) noexcept {

	generic_message(msg, Color::Modifier(Color::FG_DEFAULT));
};


// Generic function for pretty print error messages
static std::function <void(const std::string&)> error_message = [] (const std::string &msg) noexcept {
	
	generic_message(msg, Color::Modifier(Color::FG_RED));
};

// Generic function for pretty print warning messages
static std::function <void(const std::string&)> warning_message = [] (const std::string &msg) noexcept {
	
	generic_message(msg, Color::Modifier(Color::FG_BLUE));
};

} // namespace aiengine  

#endif  // SRC_MESSAGE_H_
