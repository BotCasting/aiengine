/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_FLOWFORWARDER_H_
#define SRC_FLOWFORWARDER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Flow.h"
#include "Packet.h"
#include <iostream>
#include <memory>
#include <functional>
#include <vector>
#include <algorithm>

namespace aiengine {

class Protocol;
typedef std::shared_ptr<Protocol> ProtocolPtr;

class FlowForwarder 
{
public:
    	explicit FlowForwarder() {}
    	virtual ~FlowForwarder() {}

    	void insertUpFlowForwarder(WeakPointer<FlowForwarder> ff); 
    	void addUpFlowForwarder(WeakPointer<FlowForwarder> ff);
    	void removeUpFlowForwarder(); 
    	void removeUpFlowForwarder(WeakPointer<FlowForwarder> ff); 

	void forwardFlow(Flow *flow);

        void statistics(std::basic_ostream<char>& out) const;
        void statistics() const { statistics(std::cout); };

	void setProtocol(const ProtocolPtr& proto);
	ProtocolPtr getProtocol() const { return proto_; }

	bool accept(Packet& packet) const { return check_func_(packet); }

	int64_t getTotalForwardFlows() const { return total_forward_flows_; }
	int64_t getTotalFailFlows() const { return total_fail_flows_; }
	int64_t getTotalReceivedFlows() const { return total_received_flows_; }

	void incTotalReceivedFlows() { ++total_received_flows_; }

private:
	ProtocolPtr proto_ = nullptr;
	int64_t total_received_flows_ = 0;
	int64_t total_forward_flows_ = 0;
	int64_t total_fail_flows_ = 0;
	WeakPointer<FlowForwarder> muxDown_ = WeakPointer<FlowForwarder>();
    	std::vector<WeakPointer<FlowForwarder>> flowForwarderVector_ {};
	std::function <void (Flow*)> flow_func_ = [&] (Flow*) { return; };
	std::function <bool (Packet&)> check_func_ = [&] (Packet&) { return true; };	
};

} // namespace aiengine

#endif  // SRC_FLOWFORWARDER_H_
