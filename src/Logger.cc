/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "Logger.h"

namespace aiengine {

namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;

BOOST_LOG_GLOBAL_LOGGER_INIT(logger, boost::log::sources::severity_logger_mt) {
        boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level> logger;
        std::string logfile_name("aiengine-%5N.log");

        logging::add_common_attributes();

#if defined(PYTHON_BINDING)
        // Check if there is a instance_name variabe on main

        boost::python::object main = boost::python::import("__main__");
        boost::python::dict global(main.attr("__dict__"));

        if (global.has_key("instance_name")) {
                boost::python::object obj(global.get("instance_name"));
                boost::python::extract<std::string> extractor(obj);

                if (extractor.check()) {
                        std::string instance_name = extractor();

                        logfile_name = "aiengine-" + instance_name + "-%5N.log";
                }
        }
#endif

        logging::add_file_log(
                boost::log::keywords::file_name = logfile_name.c_str(),
                boost::log::keywords::open_mode = std::ios_base::app,
                boost::log::keywords::rotation_size = 16 * 1024 * 1024,                                   /*< rotate files every 16 MiB... >*/
                boost::log::keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0), /*< ...or at midnight >*/
                boost::log::keywords::auto_flush = true,
                boost::log::keywords::format = (
                        expr::stream << "[" << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S")
                        << "] [" << expr::attr<boost::log::trivial::severity_level>("Severity") << "] "
                        << expr::smessage
                )
        );

        return logger;
}

} // namespace aiengine

