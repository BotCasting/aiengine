/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_LOGGER_H_
#define SRC_LOGGER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define BOOST_LOG_DYN_LINK 1 // necessary when linking the boost_log library dynamically
#include <boost/log/trivial.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/utility/setup.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/support/date_time.hpp>
#if defined(PYTHON_BINDING)
#include <boost/python.hpp>
#endif

namespace aiengine {

#define AITRACE BOOST_LOG_SEV(logger::get(), boost::log::trivial::trace) 
#define AIDEBUG BOOST_LOG_SEV(logger::get(), boost::log::trivial::debug) 
#define AIINFO  BOOST_LOG_SEV(logger::get(), boost::log::trivial::info)
#define AIWARN  BOOST_LOG_SEV(logger::get(), boost::log::trivial::warning)
#define AIERROR BOOST_LOG_SEV(logger::get(), boost::log::trivial::error)
#define AIFATAL BOOST_LOG_SEV(logger::get(), boost::log::trivial::fatal)

BOOST_LOG_GLOBAL_LOGGER(logger, boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level>)

} // namespace aiengine

#endif  // SRC_LOGGER_H_
