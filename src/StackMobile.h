/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 * Configuration diagram of the stack
 *
 *                         +--------------------+
 *                         | TCPGenericProtocol |                    
 *                         +-------+------------+                    
 *                                 |                                 
 *          +--------------------+ |              +--------------------+
 *          |     SSLProtocol    | |              | UDPGenericProtocol |
 *          +--------------+-----+ |              +-----------+--------+
 *                         |       |                          |      
 * +--------------------+  |       |  +--------------------+  |      
 * |    HTTPProtocol    |  |       |  |    DNSProtocol     |  |      
 * +------------------+-+  |       |  +------------+-------+  |      
 *                    |    |       |               |          |      
 *                 +--+----+-------+----+    +-----+----------+---+  
 *                 |    TCPProtocol     |    |    UDPProtocol     |  
 *                 +------------------+-+    +-+------------------+  
 *                                    |        |                     
 *      +--------------------+        |        |                     
 *      |   ICMPProtocol     +-----+  |        |                     
 *      +--------------------+     |  |        |                     
 *                               +-+--+--------+------+              
 *                               |     IPProtocol     |              
 *                               +---------+----------+              
 *                                         |                         
 *                               +---------+----------+              
 *                               |   GPRSProtocol     |              
 *                               +---------+----------+              
 *                                         |                         
 *                               +---------+----------+              
 *                               |    UDPProtocol     |              
 *                               +---------+----------+              
 *                                         |                         
 *                               +---------+----------+              
 *                         +---> |     IPProtocol     | <---+        
 *                         |     +---------+----------+     |        
 *                         |               |                |        
 *                +--------+-----------+   |   +------------+-------+
 *                |    VLANProtocol    |   |   |    MPLSProtocol    |
 *                +--------+-----------+   |   +------------+-------+
 *                         |               |                |        
 *                         |     +---------+----------+     |        
 *                         +-----+  EthernetProtocol  +-----+        
 *                               +--------------------+              
 *
 */
#ifndef SRC_STACKMOBILE_H_
#define SRC_STACKMOBILE_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <chrono>
#include <string>
#include "Multiplexer.h"
#include "FlowForwarder.h"
#include "protocols/ip/IPProtocol.h"
#include "protocols/udp/UDPProtocol.h"
#include "protocols/gprs/GPRSProtocol.h"
#include "protocols/tcp/TCPProtocol.h"
#include "protocols/icmp/ICMPProtocol.h"
#include "flow/FlowManager.h"
#include "flow/FlowCache.h"
#include "NetworkStack.h"

namespace aiengine {

class StackMobile: public NetworkStack {
public:
	explicit StackMobile();
        virtual ~StackMobile() {}

	uint16_t getLinkLayerType() const override { return ETHERTYPE_IP; }
        MultiplexerPtrWeak getLinkLayerMultiplexer() override { return mux_eth; }

        void statistics(std::basic_ostream<char> &out) const override;

	void showFlows(std::basic_ostream<char> &out, std::function<bool (const Flow&)> condition, int protocol) const override; 
	void showFlows(Json &out, std::function<bool (const Flow&)> condition, int protocol) const override; 

        void setTotalTCPFlows(int value) override;
        void setTotalUDPFlows(int value) override;
        int getTotalTCPFlows() const override;
        int getTotalUDPFlows() const override;

	void setMode(const std::string &mode) override;
	const char *getMode() const override { return operation_mode_.c_str(); }

	void setFlowsTimeout(int timeout) override;
	int getFlowsTimeout() const override { return flow_table_tcp_->getTimeout(); }

#if defined(BINDING)
        FlowManager &getTCPFlowManager() override { return *flow_table_tcp_.get(); }
        FlowManager &getUDPFlowManager() override { return *flow_table_udp_high_.get(); }
#else
        FlowManagerPtrWeak getTCPFlowManager() override { return flow_table_tcp_; }
        FlowManagerPtrWeak getUDPFlowManager() override { return flow_table_udp_high_; }
#endif

	void setTCPRegexManager(const SharedPointer<RegexManager> &rm) override;
        void setUDPRegexManager(const SharedPointer<RegexManager> &rm) override;

        void setTCPIPSetManager(const SharedPointer<IPSetManager> &ipset_mng) override;
        void setUDPIPSetManager(const SharedPointer<IPSetManager> &ipset_mng) override;

#if defined(RUBY_BINDING) || defined(LUA_BINDING) || defined(GO_BINDING)
        void setTCPRegexManager(RegexManager &rm) override { setTCPRegexManager(std::make_shared<RegexManager>(rm)); }
        void setUDPRegexManager(RegexManager &rm) override { setUDPRegexManager(std::make_shared<RegexManager>(rm)); }

        void setTCPIPSetManager(IPSetManager &ipset_mng) { setTCPIPSetManager(std::make_shared<IPSetManager>(ipset_mng)); }
        void setUDPIPSetManager(IPSetManager &ipset_mng) { setUDPIPSetManager(std::make_shared<IPSetManager>(ipset_mng)); }
#elif defined(JAVA_BINDING)
        void setTCPRegexManager(RegexManager *sig);
        void setUDPRegexManager(RegexManager *sig); 
        
	void setTCPIPSetManager(IPSetManager *ipset_mng); 
        void setUDPIPSetManager(IPSetManager *ipset_mng);
#endif

	std::tuple<Flow*, Flow*> getCurrentFlows() const override; 
	SharedPointer<Flow> getFlow(const char *ipsrc, int portsrc, int proto, const char *ipdst, int portdst) const override;
private:
	typedef NetworkStack super_;

	std::string operation_mode_ = "full";
        //Protocols
        IPProtocolPtr ip_low_ = IPProtocolPtr(new IPProtocol());
	IPProtocolPtr ip_high_ = IPProtocolPtr(new IPProtocol());
        UDPProtocolPtr udp_low_ = UDPProtocolPtr(new UDPProtocol("UDP GPRS"));
	UDPProtocolPtr udp_high_ = UDPProtocolPtr(new UDPProtocol());
        TCPProtocolPtr tcp_ = TCPProtocolPtr(new TCPProtocol());
        GPRSProtocolPtr gprs_ = GPRSProtocolPtr(new GPRSProtocol());
        ICMPProtocolPtr icmp_ = ICMPProtocolPtr(new ICMPProtocol());
	
        // Specific Multiplexers
        MultiplexerPtr mux_ip_high_ = MultiplexerPtr(new Multiplexer());
        MultiplexerPtr mux_udp_low_ = MultiplexerPtr(new Multiplexer());
	MultiplexerPtr mux_udp_high_ = MultiplexerPtr(new Multiplexer());
        MultiplexerPtr mux_gprs_ = MultiplexerPtr(new Multiplexer());
        MultiplexerPtr mux_tcp_ = MultiplexerPtr(new Multiplexer());
        MultiplexerPtr mux_icmp_ = MultiplexerPtr(new Multiplexer());

        // FlowManager and FlowCache
        FlowManagerPtr flow_table_tcp_ = FlowManagerPtr(new FlowManager());
        FlowManagerPtr flow_table_udp_high_ = FlowManagerPtr(new FlowManager());
        FlowManagerPtr flow_table_udp_low_ = FlowManagerPtr(new FlowManager());
        FlowCachePtr flow_cache_tcp_ = FlowCachePtr(new FlowCache());
        FlowCachePtr flow_cache_udp_low_ = FlowCachePtr(new FlowCache());
        FlowCachePtr flow_cache_udp_high_ = FlowCachePtr(new FlowCache());

        // FlowForwarders
        SharedPointer<FlowForwarder> ff_udp_low_ = SharedPointer<FlowForwarder>(new FlowForwarder());
        SharedPointer<FlowForwarder> ff_gprs_ = SharedPointer<FlowForwarder>(new FlowForwarder());
        SharedPointer<FlowForwarder> ff_tcp_ = SharedPointer<FlowForwarder>(new FlowForwarder());
        SharedPointer<FlowForwarder> ff_udp_high_ = SharedPointer<FlowForwarder>(new FlowForwarder());
};

} // namespace aiengine

#endif  // SRC_STACKMOBILE_H_
