#!/bin/bash
# This may require some tunning depending on the os

GO=$(which go)
SWIG=$(which swig)
GOPATHDIR=gopath/src/goaiengine
export CGO_CPPFLAGS="-I `pwd` -I `pwd`/../"
export CGO_CXXFLAGS="-DHAVE_CONFIG_H -DBINDING -DGO_BINDING -std=c++17"
export CGO_LDFLAGS="-lstdc++ -lboost_system -lboost_iostreams -lboost_log -lboost_thread -lm -lpcap -lpcre";
export GOPATH=`pwd`/gopath
if [ ! -f goaiengine.a ]; then
    (cd $GOPATHDIR && go build -x -o goaiengine.a )
    cp $GOPATHDIR/goaiengine.a .
fi
mkdir -p $GOPATH/src/goaitest
echo "Compiling Go extension"
cp goai.go $GOPATH/src/goaitest
cd $GOPATHDIR && $GO build -v -o goai.a
cd $GOPATH/src/goaitest && $GO build -v
echo "Compilation sucess"
cp $GOPATH/src/goaitest/goaitest ../../../
exit

