/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "StackOpenFlow.h"

namespace aiengine {

StackOpenFlow::StackOpenFlow() {

	setName("OpenFlow Network Stack");

	// Add all the Protocol objects
	addProtocol(eth, true);
	addProtocol(vlan, false);
	addProtocol(mpls, false);
	addProtocol(pppoe, false);
	addProtocol(ip_, true);
	addProtocol(tcp_, true);
	addProtocol(of_, true);
	addProtocol(eth_vir_, true);
	addProtocol(ip_vir_, true);
	addProtocol(tcp_vir_, true);
	addProtocol(udp_vir_, true);
	addProtocol(icmp_, true);

	// Add the L7 protocols
        addProtocol(http);
        addProtocol(ssl);
        addProtocol(smtp);
        addProtocol(imap);
        addProtocol(pop);
        addProtocol(bitcoin);
        addProtocol(tcp_generic);
        addProtocol(freqs_tcp, false);
        addProtocol(dns);
        addProtocol(sip);
        addProtocol(dhcp);
        addProtocol(ntp);
        addProtocol(snmp);
        addProtocol(ssdp);
        addProtocol(rtp);
        addProtocol(quic);
        addProtocol(udp_generic);
        addProtocol(freqs_udp, false);

	// Link the FlowCaches to their corresponding FlowManager for timeouts
	// The physic FlowManager have a 24 hours timeout
	flow_table_tcp_->setTimeout(86400);

	flow_table_tcp_->setFlowCache(flow_cache_tcp_);
	flow_table_udp_vir_->setFlowCache(flow_cache_udp_vir_);
	flow_table_tcp_vir_->setFlowCache(flow_cache_tcp_vir_);

	// Configure the Protocols with the FlowForwarders and Multiplexers
	ip_->setMultiplexer(mux_ip);
	mux_ip->setProtocol(static_cast<ProtocolPtr>(ip_));

	tcp_->setMultiplexer(mux_tcp_);
	mux_tcp_->setProtocol(static_cast<ProtocolPtr>(tcp_));
	ff_tcp_->setProtocol(static_cast<ProtocolPtr>(tcp_));

        of_->setMultiplexer(mux_of_);
        mux_of_->setProtocol(static_cast<ProtocolPtr>(of_));
        ff_of_->setProtocol(static_cast<ProtocolPtr>(of_));

        eth_vir_->setMultiplexer(mux_eth_vir_);
        mux_eth_vir_->setProtocol(static_cast<ProtocolPtr>(eth_vir_));

        ip_vir_->setMultiplexer(mux_ip_vir_);
        mux_ip_vir_->setProtocol(static_cast<ProtocolPtr>(ip_vir_));

	udp_vir_->setMultiplexer(mux_udp_vir_);
        mux_udp_vir_->setProtocol(static_cast<ProtocolPtr>(udp_vir_));
        ff_udp_vir_->setProtocol(static_cast<ProtocolPtr>(udp_vir_));

	tcp_vir_->setMultiplexer(mux_tcp_vir_);
	mux_tcp_vir_->setProtocol(static_cast<ProtocolPtr>(tcp_vir_));
	ff_tcp_vir_->setProtocol(static_cast<ProtocolPtr>(tcp_vir_));

        icmp_->setMultiplexer(mux_icmp_);
        mux_icmp_->setProtocol(static_cast<ProtocolPtr>(icmp_));

	// Configure the multiplexers of the physical side
	mux_eth->addUpMultiplexer(mux_ip);
	mux_ip->addDownMultiplexer(mux_eth);
	mux_ip->addUpMultiplexer(mux_tcp_);
	mux_tcp_->addDownMultiplexer(mux_ip);
	mux_of_->addUpMultiplexer(mux_eth_vir_);

        mux_eth_vir_->addDownMultiplexer(mux_of_);
        mux_eth_vir_->addUpMultiplexer(mux_ip_vir_);

	// configure the multiplexers of the second part
        mux_ip_vir_->addDownMultiplexer(mux_eth_vir_);
        mux_ip_vir_->addUpMultiplexer(mux_icmp_);
        mux_icmp_->addDownMultiplexer(mux_ip_vir_);
        mux_ip_vir_->addUpMultiplexer(mux_udp_vir_);
        mux_udp_vir_->addDownMultiplexer(mux_ip_vir_);
        mux_ip_vir_->addUpMultiplexer(mux_tcp_vir_);
        mux_tcp_vir_->addDownMultiplexer(mux_ip_vir_);

	// Connect the FlowManager and FlowCache
	tcp_->setFlowCache(flow_cache_tcp_);
	tcp_->setFlowManager(flow_table_tcp_);
	flow_table_tcp_->setProtocol(tcp_);

	udp_vir_->setFlowCache(flow_cache_udp_vir_);
	udp_vir_->setFlowManager(flow_table_udp_vir_);
	tcp_vir_->setFlowCache(flow_cache_tcp_vir_);
	tcp_vir_->setFlowManager(flow_table_tcp_vir_);

	flow_table_tcp_->setProtocol(tcp_);
	flow_table_tcp_vir_->setProtocol(tcp_vir_);
	flow_table_udp_vir_->setProtocol(udp_vir_);

        // Connect to upper layers the FlowManager for been able to clear the caches.
        http->setFlowManager(flow_table_tcp_vir_);
        ssl->setFlowManager(flow_table_tcp_vir_);
        smtp->setFlowManager(flow_table_tcp_vir_);
        imap->setFlowManager(flow_table_tcp_vir_);
        pop->setFlowManager(flow_table_tcp_vir_);
        dns->setFlowManager(flow_table_udp_vir_);
        sip->setFlowManager(flow_table_udp_vir_);
        ssdp->setFlowManager(flow_table_udp_vir_);

        freqs_tcp->setFlowManager(flow_table_tcp_vir_);
        freqs_udp->setFlowManager(flow_table_udp_vir_);
	// Connect the AnomalyManager with the protocols that may have anomalies
        ip_->setAnomalyManager(anomaly_);
        tcp_->setAnomalyManager(anomaly_);
        tcp_vir_->setAnomalyManager(anomaly_);
        udp_vir_->setAnomalyManager(anomaly_);

	// The low FlowManager have a 24 hours timeout to keep the Context on memory
        flow_table_tcp_->setTimeout(86400);

	// Configure the FlowForwarders
        tcp_->setFlowForwarder(ff_tcp_);
        ff_tcp_->addUpFlowForwarder(ff_of_);
        of_->setFlowForwarder(ff_of_);
        tcp_vir_->setFlowForwarder(ff_tcp_vir_);
        udp_vir_->setFlowForwarder(ff_udp_vir_);

        setTCPDefaultForwarder(ff_tcp_vir_);
        setUDPDefaultForwarder(ff_udp_vir_);

        std::ostringstream msg;
        msg << getName() << " ready.";

        infoMessage(msg.str());

	setMode("full");
}

void StackOpenFlow::showFlows(std::basic_ostream<char> &out, std::function<bool (const Flow&)> condition, int protocol) const {

        int total = flow_table_tcp_vir_->getTotalFlows() + flow_table_udp_vir_->getTotalFlows();
        total += flow_table_tcp_->getTotalFlows();
        out << "Flows on memory " << total << std::endl;

        if (protocol == IPPROTO_TCP) {
                flow_table_tcp_->showFlows(out, condition);
                flow_table_tcp_vir_->showFlows(out, condition);
        } else if (protocol == IPPROTO_UDP)
                flow_table_udp_vir_->showFlows(out, condition);
        else {
                flow_table_tcp_->showFlows(out, condition);
                flow_table_tcp_vir_->showFlows(out, condition);
                flow_table_udp_vir_->showFlows(out, condition);
	}
}

void StackOpenFlow::showFlows(Json &out, std::function<bool (const Flow&)> condition, int protocol) const {

        if (protocol == IPPROTO_TCP) {
                flow_table_tcp_->showFlows(out, condition);
                flow_table_tcp_vir_->showFlows(out, condition);
        } else if (protocol == IPPROTO_UDP)
                flow_table_udp_vir_->showFlows(out, condition);
        else {
                flow_table_tcp_->showFlows(out, condition);
                flow_table_tcp_vir_->showFlows(out, condition);
                flow_table_udp_vir_->showFlows(out, condition);
	}
}

void StackOpenFlow::statistics(std::basic_ostream<char> &out) const {

        super_::statistics(out);
}

void StackOpenFlow::setTotalTCPFlows(int value) {

	flow_cache_tcp_->createFlows(value/8);
	tcp_->createTCPInfos(value/8);

        flow_cache_tcp_vir_->createFlows(value);
	tcp_vir_->createTCPInfos(value);

	// The vast majority of the traffic of internet is HTTP
        // so create 75% of the value received for the http caches
	http->increaseAllocatedMemory(value * 0.75);

        // The 40% of the traffic is SSL
        ssl->increaseAllocatedMemory(value * 0.4);

        // 5% of the traffic could be SMTP/IMAP, im really positive :D
        smtp->increaseAllocatedMemory(value * 0.05);
        imap->increaseAllocatedMemory(value * 0.05);
        pop->increaseAllocatedMemory(value * 0.05);
        bitcoin->increaseAllocatedMemory(value * 0.05);
        freqs_tcp->increaseAllocatedMemory(16);
}

void StackOpenFlow::setTotalUDPFlows(int value) {

        flow_cache_udp_vir_->createFlows(value);
        dns->increaseAllocatedMemory(value / 2);
        sip->increaseAllocatedMemory(value * 0.2);
        ssdp->increaseAllocatedMemory(value * 0.2);
        dhcp->increaseAllocatedMemory(value * 0.1);
        quic->increaseAllocatedMemory(value * 0.1);
        freqs_udp->increaseAllocatedMemory(16);
}

int StackOpenFlow::getTotalTCPFlows() const { return flow_cache_tcp_->getTotalFlows(); }

int StackOpenFlow::getTotalUDPFlows() const { return flow_cache_udp_vir_->getTotalFlows(); }

void StackOpenFlow::setMode(const std::string &mode) {

        std::initializer_list<SharedPointer<FlowForwarder>> tcp_list {
                ff_tcp_vir_, ff_http, ff_ssl, ff_smtp, ff_imap, ff_pop, ff_bitcoin,
                ff_tcp_generic, ff_tcp_freqs};
        std::initializer_list<SharedPointer<FlowForwarder>> udp_list {
                ff_udp_vir_, ff_dns, ff_sip, ff_dhcp, ff_ntp, ff_snmp, ff_ssdp,
                ff_rtp, ff_quic, ff_udp_generic, ff_udp_freqs};

        if (auto it = modes.find(mode); it != modes.end()) {
                std::ostringstream msg;

                disableFlowForwarders(tcp_list);
                disableFlowForwarders(udp_list);

                if ((*it).second == Mode::FULL) {

			operation_mode_ = "full";
                        msg << "Enable FullEngine mode on " << getName();
                        enableFlowForwarders(tcp_list);
                        enableFlowForwarders(udp_list);

                } else if ((*it).second == Mode::FREQUENCIES) {

			operation_mode_ = "frequency";
                        msg << "Enable FrequencyEngine mode on " << getName();
                        enableFlowForwarders({ff_tcp_vir_, ff_tcp_freqs});
                        enableFlowForwarders({ff_udp_vir_, ff_udp_freqs});

                } else if ((*it).second == Mode::NIDS) {

			operation_mode_ = "nids";
                        msg << "Enable NIDSEngine mode on " << getName();
                        enableFlowForwarders({ff_tcp_vir_, ff_tcp_generic});
                        enableFlowForwarders({ff_udp_vir_, ff_udp_generic});
                }
                infoMessage(msg.str());
        }
}

void StackOpenFlow::setFlowsTimeout(int timeout) {

        flow_table_tcp_vir_->setTimeout(timeout);
        flow_table_udp_vir_->setTimeout(timeout);
}

void StackOpenFlow::setTCPRegexManager(const SharedPointer<RegexManager> &rm) {

	tcp_vir_->setRegexManager(rm);
	tcp_generic->setRegexManager(rm);
	super_::setTCPRegexManager(rm);
}

void StackOpenFlow::setUDPRegexManager(const SharedPointer<RegexManager> &rm) {

	udp_vir_->setRegexManager(rm);
	udp_generic->setRegexManager(rm);
	super_::setUDPRegexManager(rm);
}

void StackOpenFlow::setTCPIPSetManager(const SharedPointer<IPSetManager> &ipset_mng) {

	tcp_vir_->setIPSetManager(ipset_mng);
	super_::setTCPIPSetManager(ipset_mng);
}

void StackOpenFlow::setUDPIPSetManager(const SharedPointer<IPSetManager> &ipset_mng) {

	udp_vir_->setIPSetManager(ipset_mng);
	super_::setUDPIPSetManager(ipset_mng);
}

#if defined(JAVA_BINDING)

void StackOpenFlow::setTCPRegexManager(RegexManager *sig) {

        SharedPointer<RegexManager> rm;

        if (sig != nullptr)
                rm.reset(sig);

        setTCPRegexManager(rm);
}

void StackOpenFlow::setUDPRegexManager(RegexManager *sig) {

        SharedPointer<RegexManager> rm;

        if (sig != nullptr)
                rm.reset(sig);

        setUDPRegexManager(rm);
}

void StackOpenFlow::setTCPIPSetManager(IPSetManager *ipset_mng) {

        SharedPointer<IPSetManager> im;

        if (ipset_mng != nullptr)
                im.reset(ipset_mng);

        setTCPIPSetManager(im);
}

void StackOpenFlow::setUDPIPSetManager(IPSetManager *ipset_mng) {

        SharedPointer<IPSetManager> im;

        if (ipset_mng != nullptr)
                im.reset(ipset_mng);

        setUDPIPSetManager(im);
}

#endif

std::tuple<Flow*, Flow*> StackOpenFlow::getCurrentFlows() const {

        Flow *low_flow = tcp_->getCurrentFlow();
        Flow *high_flow = nullptr;
	uint16_t proto = ip_vir_->getProtocol();

        if (proto == IPPROTO_TCP)
                high_flow = tcp_vir_->getCurrentFlow();
        else if (proto == IPPROTO_UDP)
                high_flow = udp_vir_->getCurrentFlow();

#if GCC_VERSION < 50500
        return std::tuple<Flow*, Flow*>(low_flow, high_flow);
#else
        return {low_flow ,high_flow};
#endif
}

SharedPointer<Flow> StackOpenFlow::getFlow(const char *ipsrc, int portsrc, int proto, const char *ipdst, int portdst) const {

        if (proto == IPPROTO_TCP) {
                SharedPointer<Flow> flow = flow_table_tcp_->findFlow(ipsrc, portsrc, proto, ipdst, portdst);
                if (!flow)
                        flow = flow_table_tcp_vir_->findFlow(ipsrc, portsrc, proto, ipdst, portdst);

                return flow;
        } else
                return flow_table_udp_vir_->findFlow(ipsrc, portsrc, proto, ipdst, portdst);
}

} // namespace aiengine
