TEST_FLOW_OBJECTS =     ../dns/DNSInfo.cc \
                        ../sip/SIPInfo.cc \
                        ../ssdp/SSDPInfo.cc \
                        ../http/HTTPInfo.cc \
                        ../ssl/SSLInfo.cc \
                        ../smtp/SMTPInfo.cc \
                        ../tcp/TCPInfo.cc \
                        ../bitcoin/BitcoinInfo.cc \
			../gprs/GPRSInfo.cc \
                        ../coap/CoAPInfo.cc \
                        ../imap/IMAPInfo.cc \
                        ../pop/POPInfo.cc \
			../mqtt/MQTTInfo.cc \
			../netbios/NetbiosInfo.cc \
			../dhcp/DHCPInfo.cc \
			../dhcp6/DHCPv6Info.cc \
			../smb/SMBInfo.cc \
			../ssh/SSHInfo.cc \
			../dcerpc/DCERPCInfo.cc \
			../quic/QuicInfo.cc \
			../dtls/DTLSInfo.cc \
			../frequency/Frequencies.cc \
			../frequency/PacketFrequencies.cc

if IS_DARWIN
  LOGGING_FLAGS = -lboost_log-mt -lboost_thread-mt
else
  LOGGING_FLAGS = -lboost_log -lboost_thread
endif
