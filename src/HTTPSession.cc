/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "HTTPSession.h"
#include <boost/spirit/include/qi.hpp> // For parsing the 5 tuple

namespace qi = boost::spirit::qi;

namespace aiengine {

/* LCOV_EXCL_START */

SystemPtr system = SystemPtr(new System());

void HTTPSession::start() {

	do_read();
}

void HTTPSession::do_read() {

        // Make the request empty before reading,
        // otherwise the operation behavior is undefined.
        request = {};

        bhttp::async_read(socket_, buffer_, request,
        	std::bind(
                	&HTTPSession::handle_read,
                        shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2));
}

void HTTPSession::handle_read(boost::system::error_code ec, std::size_t bytes_transferred) {

        boost::ignore_unused(bytes_transferred);

        total_bytes_ += bytes_transferred;

        // This means the client closed the connection
        if (ec)
                return do_close();

        ++ total_requests_;
        last_time_ = std::time(nullptr);
        process_request(std::move(request));
}

void HTTPSession::handle_write( boost::system::error_code ec, std::size_t bytes_transferred, bool close) {

        boost::ignore_unused(bytes_transferred);

        total_bytes_ += bytes_transferred;
        ++ total_responses_;

        if (ec) {
                // std::cout << "Error on write:" << ec.message() << "\n";
                return;
        }

        if (close) {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                return do_close();
        }

        // We're done with the response so delete it
        res_ = nullptr;

        // Read another request
        do_read();
}

void HTTPSession::do_close() {

	socket_.close();
}

void HTTPSession::process_request(bhttp::request<bhttp::dynamic_body>&& req) {

        bhttp::response<bhttp::dynamic_body> response;
        response.version(req.version());
        response.keep_alive(req.keep_alive());
        response.set(bhttp::field::server, "AIEngine " VERSION);
        response.content_length(0);

	if (stack_ == nullptr) {
		std::string_view message = "No stack available";

		response.keep_alive(false);
		response.set(bhttp::field::content_type, "text/html");
		response.result(bhttp::status::service_unavailable);
		boost::beast::ostream(response.body()) << message;
		response.content_length(message.length());

        	AIINFO << req.method() << " " << req.target()
                	<< " " << response[bhttp::field::content_type] << " " << response.result_int();

		return send_response(std::move(response));
	}

	if (req.method() == bhttp::verb::get)
        	handle_get_message(response);
	else if (req.method() == bhttp::verb::post)
        	handle_post_message(response);
	else if (req.method() == bhttp::verb::put)
        	handle_put_message(response);
	else {
		response.result(bhttp::status::bad_request);
                response.set(bhttp::field::content_type, "text/plain");
                boost::beast::ostream(response.body())
                	<< "Invalid request-method '"
                        << request.method_string().to_string()
                        << "'";
        }

        AIINFO << req.method() << " " << req.target()
               	<< " " << response[bhttp::field::content_type] << " " << response.result_int();

	return send_response(std::move(response));
}

void HTTPSession::handle_get_message(bhttp::response<bhttp::dynamic_body> &response) {

	std::string uri(request.target());

	if (uri.compare(0, strlen(http_uri_show_protocols_summary), http_uri_show_protocols_summary) == 0)
		show_protocols_summary(response);
	else if (uri.compare(0, strlen(http_uri_show_network_flows), http_uri_show_network_flows) == 0)
		show_network_flows(response);
	else if (uri.compare(0, strlen(http_uri_show_protocol), http_uri_show_protocol) == 0)
		show_protocol_summary(response);
	else if (uri.compare(0, strlen(http_uri_show_summary), http_uri_show_summary) == 0)
		show_summary(response);
	else if (uri.compare(0, strlen(http_uri_show_system), http_uri_show_system) == 0)
		show_system(response);
	else if (uri.compare(0, strlen(http_uri_pcapfile), http_uri_pcapfile) == 0)
		show_pcapfile(response);
	else if (uri.compare(0, strlen(http_uri_show_network_flow), http_uri_show_network_flow) == 0)
		show_network_flow(response);
	else if (uri.compare(0, strlen(http_uri_show_current_packet), http_uri_show_current_packet) == 0)
		show_current_packet(response);
#if defined(PYTHON_BINDING)
	else if (uri.compare(0, strlen(http_uri_python_script), http_uri_python_script) == 0)
		show_python_script(response);
	else if (uri.compare(0, strlen(http_uri_show_python_globals), http_uri_show_python_globals) == 0)
		show_python_globals(response);
#endif
	else
		show_uris(response);
}

void HTTPSession::handle_post_message(bhttp::response<bhttp::dynamic_body> &response) {

        std::string uri(request.target());

        if (uri.compare(0, strlen(http_uri_pcapfile), http_uri_pcapfile) == 0)
                upload_pcapfile(response);
#if defined(PYTHON_BINDING)
        else if (uri.compare(0, strlen(http_uri_python_script), http_uri_python_script) == 0)
                upload_python_script(response);
#endif
}

void HTTPSession::show_protocols_summary(bhttp::response<bhttp::dynamic_body> &response) {

	std::ostringstream out;

        response.set(bhttp::field::content_type, "text/plain");

	if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0) {
        	if (std::size_t found = accept.find("application/json"); found != std::string::npos) {
                	Json j;
                        response.set(bhttp::field::content_type, "application/json");
			stack_->showProtocolSummary(j);
                        out << j;
                } else
			stack_->showProtocolSummary(out);
	} else
		stack_->showProtocolSummary(out);

	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

// The network flows uri looks like
// /aiengine/flows
// /aiengine/flows/100
// /aiengine/flows/dns/100
// /aiengine/flows/ssl

void HTTPSession::show_network_flows(bhttp::response<bhttp::dynamic_body> &response) {

	std::ostringstream out;
	std::string uri(request.target());
	bool jsonize = false;
	UserFlowOptions options;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
			jsonize = true;

	uri.erase(0, strlen(http_uri_show_network_flows));

	// Check if the URI contains parameters
	if (uri.find("?") != std::string::npos) {
		std::vector<std::string> parameters;

		uri.erase(std::remove(uri.begin(), uri.end(), '?'), uri.end());

		boost::split(parameters, uri, boost::is_any_of("&"));

		for (auto &parameter: parameters) {
			std::vector<std::string> entry;

			boost::split(entry, parameter, boost::is_any_of("="));

			if ((entry.size() == 2) and (entry[1].length() > 0)) {

				if (entry[0].compare("src_ip") == 0) {
					options.portsrc = std::atoi(entry[1].c_str());
					continue;
				}

				if (entry[0].compare("dst_port") == 0) {
					options.portdst = std::atoi(entry[1].c_str());
					continue;
				}

				if (entry[0].compare("src_ip") == 0) {
					options.ipsrc = entry[1];
					continue;
				}

				if (entry[0].compare("dst_ip") == 0) {
					options.ipdst = entry[1];
					continue;
				}

				if (entry[0].compare("protocol") == 0) {
					options.protocol = std::atoi(entry[1].c_str());
					continue;
				}

				if (entry[0].compare("limit") == 0) {
					options.limit = std::atoi(entry[1].c_str());
					continue;
				}

				if (entry[0].compare("l7protocol_name") == 0) {
					options.l7protocol_name = entry[1];
					continue;
				}
                        }
		}
	}

	if (jsonize) {
        	Json j;
                response.set(bhttp::field::content_type, "application/json");

		stack_->showFlows(j, options);

                out << j;
	} else {
        	response.set(bhttp::field::content_type, "text/plain");

		stack_->showFlows(out, options);
	}

	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

// The protocol uri looks like
// /aiengine/protocol/ssl
// /aiengine/protocol/ssl/1
// /aiengine/protocol/ssl/map/<name>

void HTTPSession::show_protocol_summary(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
        std::vector<std::string> items;
        std::string uri(request.target());
        int level = 1;
        std::string protocol = "";
	std::string map_name = "";
        bool jsonize = false;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        jsonize = true;

        uri.erase(0, strlen(http_uri_show_protocol));

        boost::split(items, uri, boost::is_any_of("/"));

        if (items.size() > 1) {
                protocol = items[1];

		if (items.size() > 2) {
			if (int32_t value = std::atoi(items[2].c_str()); value > 0)
                        	level = value;
			else if (items[2].compare("map") == 0)
				if (items.size() > 3)
					map_name = items[3];
		}
        }

        if (jsonize) {
                Json j;
                response.set(bhttp::field::content_type, "application/json");

		if (map_name.length() > 0)
			stack_->statistics(j, protocol, map_name);
		else
			stack_->statistics(j, protocol, level);

                out << j;
        } else {
                response.set(bhttp::field::content_type, "text/plain");
		stack_->statistics(out, protocol, level);
	}

	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

// /aiengine/summary

void HTTPSession::show_summary(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
        bool jsonize = false;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        jsonize = true;

        if (jsonize) {
                Json j;
                response.set(bhttp::field::content_type, "application/json");

		pdis_->statistics(j);

                out << j;
        } else {
                response.set(bhttp::field::content_type, "text/plain");
		pdis_->statistics(out);
	}

	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

// /aiengine/system
void HTTPSession::show_system(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
        bool jsonize = false;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        jsonize = true;

        if (jsonize) {
                Json j;
                response.set(bhttp::field::content_type, "application/json");

		system->statistics(j);

                out << j;
        } else {
                response.set(bhttp::field::content_type, "text/plain");
		system->statistics(out);
	}

	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

void HTTPSession::show_uris(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
        bool jsonize = false;
	std::ostringstream baseuri;

	baseuri << "http://" << socket_.local_endpoint().address().to_string();
	baseuri << ":" << socket_.local_endpoint().port();

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        jsonize = true;

        if (jsonize) {
                nlohmann::json j;
                response.set(bhttp::field::content_type, "application/json");

		j["uris"] = {
			{ std::string(baseuri.str() + http_uri_show_protocols_summary), "Protocols summary" },
			{ std::string(baseuri.str() + http_uri_show_protocol), "Protocol summary" },
			{ std::string(baseuri.str() + http_uri_show_network_flows), "Network flows" },
			{ std::string(baseuri.str() + http_uri_show_summary), "Summary" },
			{ std::string(baseuri.str() + http_uri_show_system), "System" },
			{ std::string(baseuri.str() + http_uri_pcapfile), "Upload pcapfile" },
#if defined(PYTHON_BINDING)
			{ std::string(baseuri.str() + http_uri_python_script), "Upload python code" },
			{ std::string(baseuri.str() + http_uri_show_python_globals), "Python globals" },
#endif
			{ std::string(baseuri.str() + http_uri_show_network_flow), "Network flow" },
			{ std::string(baseuri.str() + http_uri_show_current_packet), "Current packet" }
		};

                out << j;
        } else {
                response.set(bhttp::field::content_type, "text/html");

		out << "<html><head><title>AIEngine operations</title></head>\n<body><center>\n";

		out << "<a href=\"" << baseuri.str() << http_uri_show_protocols_summary << "\">Protocols summary</a><br>\n";
		out << "<a href=\"" << baseuri.str() << http_uri_show_protocol << "\">Protocol summary</a><br>\n";
		out << "<a href=\"" << baseuri.str() << http_uri_show_network_flows << "\">Network flows</a><br>\n";
		out << "<a href=\"" << baseuri.str() << http_uri_show_summary << "\">Summary</a><br>\n";
		out << "<a href=\"" << baseuri.str() << http_uri_show_system << "\">System</a><br>\n";
		out << "<a href=\"" << baseuri.str() << http_uri_pcapfile << "\">Upload pcapfile</a><br>\n";
#if defined(PYTHON_BINDING)
		out << "<a href=\"" << baseuri.str() << http_uri_python_script << "\">Python code</a><br>\n";
		out << "<a href=\"" << baseuri.str() << http_uri_show_python_globals << "\">Python globals</a><br>\n";
#endif
		out << "<a href=\"" << baseuri.str() << http_uri_show_network_flow << "\">Network flow</a><br>\n";
		out << "<a href=\"" << baseuri.str() << http_uri_show_current_packet << "\">Current packet</a><br>\n";
		out << "</center></body>\n</html>\n";
	}

	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

void HTTPSession::show_pcapfile(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;

        response.set(bhttp::field::content_type, "text/html");

        out << "<html><head><title>Upload pcapfile</title></head>\n<body><center>\n";

        out << "<form id=\"uploadbanner\" enctype=\"multipart/form-data\" method=\"post\" >\n";
        out << " <input id=\"fileupload\" name=\"myfile\" type=\"file\" />\n";
        out << "<input type=\"submit\" value=\"submit\" id=\"submit\" />\n";
        out << "</form>\n";

        out << "</center></body>\n</html>\n";

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

#if defined(PYTHON_BINDING)

void HTTPSession::show_python_script(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;

        response.set(bhttp::field::content_type, "text/html");

        out << "<html><head><title>Python code</title></head>\n<body><center>\n";

        out << "<form id=\"uploadbanner\" enctype=\"multipart/form-data\" method=\"post\" >\n";
	out << "<label>Python code</label><br>\n";
	out << "<textarea type=\"text\" id=\"code\" name=\"code\" rows=\"32\" cols=\"64\">\n";
	out << "</textarea><br>\n";
        out << "<input type=\"submit\" value=\"submit\" id=\"submit\" />\n";
        out << "</form>\n";

        out << "</center></body>\n</html>\n";

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

void HTTPSession::show_python_globals(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
        bool jsonize = false;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        jsonize = true;

        boost::python::object main = boost::python::import("__main__");
        boost::python::dict global(main.attr("__dict__"));

	std::map<std::string, std::string> items;

	boost::python::list keys = boost::python::list(global.keys());
	for (int i = 0; i < len(keys); ++i) {
		boost::python::extract<std::string> extractor(keys[i]);
		if (extractor.check()) {
			std::string key = extractor();

			boost::python::extract<boost::python::object> objectExtractor(global[key]);
			boost::python::object obj = objectExtractor();
			std::string object_classname = boost::python::extract<std::string>(obj.attr("__class__").attr("__name__"));

			if (key.rfind("__", 0) != 0)
				items[key] = object_classname;
		}
	}

	if (jsonize) {
                nlohmann::json j;
                response.set(bhttp::field::content_type, "application/json");

		j["objects"] = items;

		out << j;
	} else {
        	response.set(bhttp::field::content_type, "text/plain");
		out << "Python objects" << "\n";

		for (auto &item: items)
			out << "\t" << item.first << ":" << item.second << "\n";
	}

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

#endif

// Now POST methods

void HTTPSession::upload_pcapfile(bhttp::response<bhttp::dynamic_body> &response) {

        std::size_t off, roff, foff_s, foff_e;
        boost::beast::string_view ct = request[bhttp::field::content_type];
        response.content_length(0);

        if (off = ct.find("multipart/form-data"); off == std::string::npos) {
                response.result(bhttp::status::unsupported_media_type);
                return;
        }

        if (off = ct.find("boundary="); off == std::string::npos) {
                response.result(bhttp::status::bad_request);
                return;
        }

        // Now get the filename and the content type to verify that is a pcap file

        std::ostringstream body;
#if BOOST_BEAST_VERSION >= 266
        body << boost::beast::make_printable(request.body().data());
#else
        body << boost::beast::buffers(request.body().data());
#endif
        std::string boundary(ct.substr(off + 9));

        if (off = body.str().find(boundary); off == std::string::npos) {
                response.result(bhttp::status::bad_request);
                return;
        }

        off += boundary.length() + 2;

        if (off = body.str().find("\r\n\r\n", off); off == std::string::npos) {
                response.result(bhttp::status::bad_request);
                return;
        }

        if (roff = body.str().rfind(boundary); roff == std::string::npos) {
                response.result(bhttp::status::bad_request);
                return;
        }

        // extract the file name
        if (foff_s = body.str().find("filename=", boundary.length() + 4); foff_s == std::string::npos) {
                response.result(bhttp::status::bad_request);
                return;
        }

        if (foff_e = body.str().find("\r\n", foff_s); foff_e == std::string::npos) {
                response.result(bhttp::status::bad_request);
                return;
        }

        std::string filename(body.str().substr(foff_s + 10, foff_e - (foff_s + 10) - 1));
        std::ostringstream path;

        path << "/tmp/" << filename;
        std::ofstream ofs (path.str(), std::ofstream::out);

        int len = body.str().length() - (off + 4 + boundary.length() + 2 + 6);
        ofs << body.str().substr(off + 4, len);

        ofs.close();

        // Close the other device or pcapfiles
        pdis_->close();

        // Open the new one
        pdis_->open(path.str());

        // Run it
        pdis_->run();

        std::ostringstream redirect;

        redirect << "http://" << socket_.local_endpoint().address() << ":" << socket_.local_endpoint().port();
        redirect << http_uri_show_uris;

        response.set(bhttp::field::location, redirect.str());
        response.result(bhttp::status::found);
}

#if defined(PYTHON_BINDING)

void HTTPSession::upload_python_script(bhttp::response<bhttp::dynamic_body> &response) {

        std::size_t off, roff;
        boost::beast::string_view ct = request[bhttp::field::content_type];
	std::string code;
        std::ostringstream body, out;
#if BOOST_BEAST_VERSION >= 266
        body << boost::beast::make_printable(request.body().data());
#else
        body << boost::beast::buffers(request.body().data());
#endif
        response.content_length(0);

        if (off = ct.find("multipart/form-data"); off != std::string::npos) {
		if (off = ct.find("boundary="); off == std::string::npos) {
                	response.result(bhttp::status::bad_request);
                	return;
        	}
        	std::string boundary(ct.substr(off + 9));

        	if (off = body.str().find(boundary); off == std::string::npos) {
                	response.result(bhttp::status::bad_request);
                	return;
        	}

		// Just jump to the next line and dont process Content-disposition
		if (off = body.str().find("\r\n\r\n", off); off == std::string::npos) {
                	response.result(bhttp::status::bad_request);
                	return;
		}

        	if (roff = body.str().rfind(boundary); roff == std::string::npos) {
                	response.result(bhttp::status::bad_request);
                	return;
        	}

		code = body.str().substr(off, roff - off - 4);

        } else if (off = ct.find("text/python"); off != std::string::npos) {
		code = body.str();
	} else {
                response.result(bhttp::status::unsupported_media_type);
                return;
	}

	if (code.length() > 0) {
        	std::ostringstream filename, scode;
		int pid = getpid();

		filename << "/tmp/aiengine.pcode." << pid;
#if defined(IS_DARWIN)
		boost::system::error_code ec;

		boost::filesystem::resize_file(filename.str(), 0, ec); // truncate file
#else
		std::error_code ec;

		std::filesystem::resize_file(filename.str(), 0, ec); // truncate file
#endif
		// We create a execution with a redirection of the output
		// bear in mind that the user could put print statements
		// so we need to redirect properly the things
		scode << "import sys\n";
		scode << "output_" << pid << " = open('" << filename.str() << "', 'w')\n";
		scode << "temp_" << pid << " = sys.stdout\n";
		scode << "sys.stdout = output_" << pid << "\n";
		scode << code << "\n";
                scode << "output_" << pid << ".close()\n";
                scode << "sys.stdout = temp_" << pid ;

               	std::ofstream term(filename.str(), std::ios_base::out);
		auto &oldout = OutputManager::getInstance()->out();
                OutputManager::getInstance()->setOutput(term);

        	try {
                	[[maybe_unused]] PyGilContext gil_lock;

                	// Retrieve the main module.
                	boost::python::object main = boost::python::import("__main__");
                	// Retrieve the main module's namespace
                	boost::python::object global(main.attr("__dict__"));

                	boost::python::exec(scode.str().c_str(), global);
        	} catch (boost::python::error_already_set const &) {
			PyObject *ptype, *pvalue, *ptraceback;
			PyErr_Fetch(&ptype, &pvalue, &ptraceback);

			std::string error_msg = boost::python::extract<std::string>(pvalue);

			term << error_msg << std::endl;
        	}

		term.close();
                OutputManager::getInstance()->setOutput(oldout);

		std::ifstream t(filename.str());

		out << t.rdbuf();

		t.close();
        }

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
        response.result(bhttp::status::ok);
}

#endif

SharedPointer<Flow> HTTPSession::get_flow_from_uri(const std::string &uri) const {

        std::vector<std::string> items;
        std::string address;
	SharedPointer<Flow> flow = nullptr;

        boost::split(items, uri, boost::is_any_of("/"));

        if (!items.empty())
                address = items.back();

        std::string addrsrc, addrdst;
        int portsrc, portdst, proto;

	// The URI can have encoded charaters for [] so need to replace them
	boost::replace_all(address, "%5B", "[");
	boost::replace_all(address, "%5D", "]");

        // Do a basic parsing
        qi::phrase_parse(address.begin(), address.end(),
                qi::lit("[") >> *(~qi::char_("") - "]") >> qi::lit("]:") >>
                qi::int_ >> qi::lit(":[") >> *(~qi::char_("") - "]"),
                qi::ascii::space,
                addrsrc, proto, addrdst);

        std::size_t found = addrsrc.find_last_of(":");

        std::string ipsrc = addrsrc.substr(0, found);
        portsrc = std::atoi(std::string(addrsrc.substr(found + 1)).c_str());

        found = addrdst.find_last_of(":");

        std::string ipdst = addrdst.substr(0, found);
        portdst = std::atoi(std::string(addrdst.substr(found + 1)).c_str());

        if ((ipsrc.length() > 0)and(portsrc > 0)and(ipdst.length() > 0)and(portdst > 0))
                flow = stack_->getFlow(ipsrc.c_str(), portsrc, proto, ipdst.c_str(), portdst);

	return flow;
}

// URIS:
// /aiengine/flow/<id>
// /aiengine/flow/[10.42.0.42:42140]:6:[172.217.7.14:443]

void HTTPSession::show_network_flow(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
        std::vector<std::string> items;
        std::string uri(request.target());
        bool jsonize = false;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        jsonize = true;

        uri.erase(0, strlen(http_uri_show_network_flow));

	if (SharedPointer<Flow> flow = get_flow_from_uri(uri); flow) {
        	response.result(bhttp::status::ok);
		std::time_t duration = std::time(nullptr) - flow->getArriveTime();
		std::time_t last_packet_seen = std::time(nullptr) - flow->getLastPacketTime();
		int timeout = stack_->getFlowsTimeout();;
		std::string_view status = "active";

		if (last_packet_seen > timeout) {
			if (last_packet_seen > timeout * 2)
				status = "comatose";
			else
				status = "timeout";
		}

		if (jsonize) {
			Json j;
			response.set(bhttp::field::content_type, "application/json");

			flow->show(j);

			j["status"] = status;
			j["duration"] = (int)duration;
			j["last_packet_seen"] = (int)last_packet_seen;
			out << j;
		} else {
			response.set(bhttp::field::content_type, "text/plain");
			std::string_view ev = flow->haveEvidence() ? "yes": "no";
			std::string_view reject = flow->isReject() ? "yes": "no";
 
			out << "Network flow:" << *flow << "\n";
			out << "Protocol:" << flow->getL7ProtocolName() << "\n";
			out << "Evidence:" << ev << "\n";
			out << "Reject:" << reject << "\n";
			out << "Status:" << status << "\n";
			out << "Up bytes:" << flow->total_bytes[static_cast<int>(FlowDirection::FORWARD)] << "\n";
			out << "Down bytes:" << flow->total_bytes[static_cast<int>(FlowDirection::BACKWARD)] << "\n";
			out << "Up packets:" << flow->total_packets[static_cast<int>(FlowDirection::FORWARD)] << "\n";
			out << "Down packets:" << flow->total_packets[static_cast<int>(FlowDirection::BACKWARD)] << "\n";
			out << "Duration:" << (int)duration << " secs\n";
			out << "Last packet seen:" << (int)last_packet_seen << " secs\n";
			flow->show(out);
		}
	} else
                response.result(bhttp::status::not_found);

	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
	response.content_length(out.str().length());
}

void HTTPSession::handle_put_message(bhttp::response<bhttp::dynamic_body> &response) {

        std::string uri(request.target());

        if (uri.compare(0, strlen(http_uri_show_network_flow), http_uri_show_network_flow) == 0)
                update_network_flow(response);
}

// Just json format is allowed
void HTTPSession::update_network_flow(bhttp::response<bhttp::dynamic_body> &response) {

        boost::beast::string_view content_type = request[bhttp::field::content_type];

        if (content_type.compare("application/json") == 0) {
		std::string uri(request.target());
                std::ostringstream bodydata;

#if BOOST_BEAST_VERSION >= 266
        	bodydata << boost::beast::make_printable(request.body().data());
#else
        	bodydata << boost::beast::buffers(request.body().data());
#endif
        	uri.erase(0, strlen(http_uri_show_network_flow));

        	if (SharedPointer<Flow> flow = get_flow_from_uri(uri); flow) {
                	response.result(bhttp::status::ok);

                	nlohmann::json j = nlohmann::json::parse(bodydata.str(), nullptr, false);
                	if (j.is_discarded()) {
                        	std::string_view message = "Not valid json content";

                        	response.set(bhttp::field::content_type, "text/html");
                        	response.result(bhttp::status::bad_request);
                        	boost::beast::ostream(response.body()) << message;
                        	response.content_length(message.length());
                	} else {
				// The user wants to change the label of the flow
				if (j.find("label") != j.end(); j["label"].is_string())
					flow->setLabel(j["label"]);

				// The user wants to have evidences of the flow
				if (j.find("evidence") != j.end(); j["evidence"].is_boolean())
					flow->setEvidence(j["evidence"]);

				// The user wants to reject the flow if supported
				if (j.find("reject") != j.end(); j["reject"].is_boolean())
					flow->setReject(j["reject"]);
#if defined(PYTHON_BINDING)
				// The user wants to detach the flow
				if (j.find("detach") != j.end(); j["detach"].is_boolean())
					if (j["detach"] == true)
						flow->detach();
#endif
			}
		} else
                	response.result(bhttp::status::not_found);
	} else
		response.result(bhttp::status::unsupported_media_type);
}

void HTTPSession::show_current_packet(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
        std::vector<std::string> items;
        std::string uri(request.target());
        bool jsonize = false;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0)
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        jsonize = true;

        if (jsonize) {
		Json j;
                response.set(bhttp::field::content_type, "application/json");

		pdis_->showCurrentPayloadPacket(j);
		out << j;
	} else {
		response.set(bhttp::field::content_type, "text/plain");

		pdis_->showCurrentPayloadPacket(out);
        }

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        response.content_length(out.str().length());
}

/* LCOV_EXCL_STOP */

} // namespace aiengine
