/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#ifndef SRC_PACKETDISPATCHER_H_
#define SRC_PACKETDISPATCHER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <chrono>
#include <iomanip>
#include <pcap.h>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind/bind.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/version.hpp>
#include <exception>
#include <sys/resource.h>
#include "NetworkStack.h"
#include "Multiplexer.h"
#include "protocols/ethernet/EthernetProtocol.h"
#include "Protocol.h"
#include "StackLan.h"
#include "StackMobile.h"
#include "StackLanIPv6.h"
#include "StackVirtual.h"
#include "StackOpenFlow.h"
#include "StackMobileIPv6.h"
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
#include "Interpreter.h"
#include "TimerManager.h"
#endif
#include "EvidenceManager.h"
#include "OutputManager.h"
#include "Message.h"
#include "HTTPSession.h"
#include "Logger.h"

#if !defined(PCAP_NETMASK_UNKNOWN)
/*
 *  This value depending on the pcap library is defined or not
 *
 */
#define PCAP_NETMASK_UNKNOWN    0xFFFFFFFF
#endif

namespace aiengine {

#define PACKET_RECVBUFSIZE    4096    // receive_from buffer size for a single datagram
#define PCAP_BUFFER_SIZE      4194304 // Default 4Mb for pcap buffer size

#define BOOST_ASIO_DISABLE_EPOLL

typedef boost::asio::posix::stream_descriptor PcapStream;
typedef std::shared_ptr<PcapStream> PcapStreamPtr;

class PacketDispatcher {
public:

	enum class PacketDispatcherStatus : short {
        	RUNNING = 0,
        	STOPPED
	};

	class Statistics {
	public:
		explicit Statistics() {
			last_total_packets_sample = 0;
			last_total_bytes_sample = 0;
			std::time(&packet_time);
			std::time(&last_packet_time);
			std::time(&first_packet_time);
		}
		virtual ~Statistics() {}

		// The variables are mutable because we change when the user prints the packetdispatcher,
		// just to avoid compute the on the packet processing and better when the user wants the info.
		mutable std::time_t packet_time;
		mutable std::time_t last_packet_time;
		mutable std::time_t first_packet_time; // For measure time of pcap files;
		mutable int64_t last_total_packets_sample;
		mutable int64_t last_total_bytes_sample;
	};

	explicit PacketDispatcher(const std::string &source, int buffer_size);
	explicit PacketDispatcher(const std::string &source):
		PacketDispatcher(source, PCAP_BUFFER_SIZE) {}
	explicit PacketDispatcher():PacketDispatcher("") {}

    	virtual ~PacketDispatcher();

	void open(const std::string &source);
	void open(const std::string &source, int buffer_size);
	void run(void);
	void close(void);
    	void stop(void) { io_service_.stop(); }
	void setPcapFilter(const char *filter);
	const char *getPcapFilter() const { return pcap_filter_.c_str(); }
	void status(void);
	const char *getStackName() const { return stack_name_.c_str(); }
	int getPcapFilesDuration() const { return pcap_files_duration_; }

	void setEvidences(bool value);
	bool getEvidences() const { return have_evidences_; }

	void statistics();
	void statistics(std::basic_ostream<char> &out) const;
	void statistics(Json &out) const;

#if defined(STAND_ALONE_TEST) || defined(TESTING)
	// Use for the tests, limits the number of packets injected
	void setMaxPackets(int packets);
	void setFromPackets(int packets);
	const char *getEvidencesFilename() const { return em_->getFilename(); }
#endif

	// Enables/Disables a HTTP server for retrieve JSON information
	void setHTTPPort(int port);
	int getHTTPPort() const;

#if defined(PYTHON_BINDING)

	// For implement the 'with' statement in python needs the methods __enter__ and __exit__
	PacketDispatcher& __enter__();
	bool __exit__(boost::python::object type, boost::python::object val, boost::python::object traceback);

	void forwardPacket(const std::string &packet, int length);
	void addTimer(PyObject *callback, int seconds);
	void removeTimer(int seconds);

	void setStack(const boost::python::object &stack);
	boost::python::object getStack() const { return pystack_; }

	const char *getStatus() const;

        // The flow have been marked as accept or drop (for external Firewall integration (Netfilter))
        bool isPacketAccepted() const { return current_packet_.isAccept(); }

	void addAuthorizedIPAddresses(const boost::python::list &items);
	boost::python::list getAuthorizedIPAddresses() const;

	void showSystemProcessInformation() const;
#else
        void setStack(StackLan &stack);
        void setStack(StackMobile &stack);
        void setStack(StackLanIPv6 &stack);
        void setStack(StackVirtual &stack);
        void setStack(StackOpenFlow &stack);
        void setStack(StackMobileIPv6 &stack);
#endif

	int64_t getTotalBytes(void) const { return total_bytes_; }
	int64_t getTotalPackets(void) const { return total_packets_; }

	// This only works with real network devices
	int64_t getTotalReceivedPackets(void) const;
	int64_t getTotalDroppedPackets(void) const;
	int64_t getTotalIfDroppedPackets(void) const;

	void setStack(const SharedPointer<NetworkStack> &stack);

	void setDefaultMultiplexer(MultiplexerPtr mux); // just use for the unit tests
	void setIdleFunction(std::function <void ()> idle_function) { idle_function_ = idle_function; }

	friend std::ostream& operator<< (std::ostream &out, const PacketDispatcher &pd);

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING)
	void setShell(bool enable);
	bool getShell() const;

	void setLogUserCommands(bool enable);
	bool getLogUserCommands() const;
#endif

#if defined(LUA_BINDING)
	void setShell(lua_State *L, bool enable);
	bool getShell() const;
	void addTimer(lua_State* L, const char *callback, int seconds);
#endif

#if defined(RUBY_BINDING)
	void addTimer(VALUE callback, int seconds);
#endif
	void showCurrentPayloadPacket(std::basic_ostream<char>& out) const;
	void showCurrentPayloadPacket(Json &out) const;
	void showCurrentPayloadPacket() const;

	void addAuthorizedIPAddress(const std::string &addr);

private:
	void set_stack(const SharedPointer<NetworkStack> &stack);
	void start_read_ethernet_network(void);
	void start_read_raw_network(void);
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
	void start_read_user_input(void);
#endif
	void accept_http_connections();
	void read_ethernet_network(boost::system::error_code error);
	void read_raw_network(boost::system::error_code error);
	void forward_current_ethernet_packet();
	void forward_current_raw_packet();
	void restart_timer(int seconds);
        void open_device(const std::string &device, int buffer_size);
        void close_device(void);
        void open_pcap_file(const std::string &filename);
        void close_pcap_file(void);
        void run_device(void);
        void run_pcap(void);
	void info_message(const std::string &msg);
	void error_message(const std::string &msg);
	int get_mtu_of_network_device(const std::string &name);
	void compute_packets_bytes_rate(int64_t &packets_per_second, int64_t &bytes_per_second) const;
	bool is_ethernet_present() const;

	PacketDispatcherStatus status_ = PacketDispatcherStatus::STOPPED;
	PcapStreamPtr stream_ {};
	bool pcap_file_ready_ = false;
	bool read_in_progress_ = false;
	bool device_is_ready_ = false;
	bool have_evidences_ = false;
#if defined(STAND_ALONE_TEST) || defined(TESTING)
	int32_t max_packets_ = std::numeric_limits<int32_t>::max();
	int32_t from_packets_ = 0;
#endif
	int64_t total_packets_ = 0;
	int64_t total_bytes_ = 0;
    	pcap_t* pcap_ = nullptr;
	boost::asio::io_service io_service_;
	boost::asio::signal_set signals_;
	Statistics stats_ {};
	struct pcap_pkthdr *packet_header_ = nullptr;
	const uint8_t *packet_ = nullptr;
	int16_t packet_length_ = 0;
	bool io_running_ = false;
#if !defined(LUA_BINDING) && !defined(RUBY_BINDING) && !defined(JAVA_BINDING) && !defined(GO_BINDING)
	std::function <void ()> idle_function_ = [&] (void) {};
#else
	std::function <void ()> idle_function_;
#endif
	EthernetProtocolPtr eth_ = nullptr;
	Packet current_packet_ {};
	MultiplexerPtr defMux_= nullptr;
	std::string stack_name_ = "";
	std::string input_name_ = "";
	std::string pcap_filter_ = "";
	int pcap_buffer_size_ = PCAP_BUFFER_SIZE;
	int pcap_files_duration_ = 0; // Number of seconds of duration of the pcap files
	long pcap_file_header_offset_ = 0;

	SharedPointer<EvidenceManager> em_ = SharedPointer<EvidenceManager>(new EvidenceManager());
	SharedPointer<NetworkStack> current_network_stack_ = nullptr;
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
	SharedPointer<TimerManager> tm_ = SharedPointer<TimerManager>(new TimerManager(io_service_));
	SharedPointer<Interpreter> user_shell_ = SharedPointer<Interpreter>(new Interpreter(io_service_));
#if defined(PYTHON_BINDING)
	boost::python::object pystack_ {};
#endif
#endif
	boost::asio::ip::tcp::socket http_socket_ { io_service_ };
	boost::asio::ip::tcp::acceptor http_acceptor_ { io_service_ };
	boost::asio::ip::address http_ip_address_ = boost::asio::ip::make_address("127.0.0.1");
	// List of IP address that can check the HTTP interface
        std::set<std::string> allow_address_ {};
};

typedef std::shared_ptr<PacketDispatcher> PacketDispatcherPtr;

} // namespace aiengine

#endif  // SRC_PACKETDISPATCHER_H_
