/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#ifndef SRC_HTTPSESSION_H_
#define SRC_HTTPSESSION_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio.hpp>
#if !defined(IS_DARWIN)
#include <filesystem>
#else
#include <boost/filesystem.hpp>
#endif
#include "Pointer.h"
#include "json.hpp"
#include "NetworkStack.h"
#include "PacketDispatcher.h"
#include "System.h"

using tcp = boost::asio::ip::tcp;       // from <boost/asio.hpp>
namespace bhttp = boost::beast::http;    // from <boost/beast/http.hpp>

namespace aiengine {

#define BASE_URI "/aiengine"

/* LCOV_EXCL_START */

class PacketDispatcher;

class HTTPSession : public std::enable_shared_from_this<HTTPSession>
{
public:
        explicit HTTPSession(tcp::socket socket, const SharedPointer<NetworkStack> &stack, PacketDispatcher *pdis):
                send_response(*this),
                socket_(std::move(socket)),
		stack_(stack),
		pdis_(pdis)
                {}

        virtual ~HTTPSession() { socket_.close(); }

	char const *http_uri_show_protocols_summary = BASE_URI "/protocols/summary";
	char const *http_uri_show_protocol = BASE_URI "/protocol";
	char const *http_uri_show_network_flows = BASE_URI "/flows";
	char const *http_uri_show_summary = BASE_URI "/summary";
	char const *http_uri_show_system = BASE_URI "/system";
	char const *http_uri_show_uris = BASE_URI "/uris";
	char const *http_uri_pcapfile = BASE_URI "/pcapfile";
	char const *http_uri_python_script = BASE_URI "/python_code";
	char const *http_uri_show_network_flow = BASE_URI "/flow";
	char const *http_uri_show_python_globals = BASE_URI "/globals";
	char const *http_uri_show_current_packet = BASE_URI "/current_packet";

        void start();

        int32_t getTotalRequests() const { return total_requests_; }
        int32_t getTotalResponses() const { return total_responses_; }
        int64_t getTotalBytes() const { return total_bytes_; }
        std::time_t getLastTime() const { return last_time_; }

        // This is the C++11 equivalent of a generic lambda.
        // The function object is used to send an HTTP response message.
        struct send_lambda {
                HTTPSession& self_;

                explicit send_lambda(HTTPSession& self) : self_(self) {}

                template<bool isRequest, class Body, class Fields>
                void operator()(bhttp::message<isRequest, Body, Fields>&& msg) const {
                        // The lifetime of the message has to extend
                        // for the duration of the async operation so
                        // we use a shared_ptr to manage it.
                        auto sp = std::make_shared<
                                bhttp::message<isRequest, Body, Fields>>(std::move(msg));

                        // Store a type-erased version of the shared
                        // pointer in the class to keep it alive.
                        self_.res_ = sp;

                        bhttp::async_write(self_.socket_, *sp,
                        	std::bind(
                                	&HTTPSession::handle_write,
                                        self_.shared_from_this(),
                                        std::placeholders::_1,
                                        std::placeholders::_2,
                                        sp->need_eof()));
                }
	};

private:
	SharedPointer<Flow> get_flow_from_uri(const std::string &uri) const;

        void handle_write(boost::system::error_code ec, std::size_t bytes_transferred, bool close);
        void handle_read(boost::system::error_code ec, std::size_t bytes_transferred);
        void do_close();
        void do_read();
	void process_request(bhttp::request<bhttp::dynamic_body>&& req);

	void handle_get_message(bhttp::response<bhttp::dynamic_body> &response);
	void show_protocols_summary(bhttp::response<bhttp::dynamic_body> &response);
	void show_protocol_summary(bhttp::response<bhttp::dynamic_body> &response);
	void show_network_flows(bhttp::response<bhttp::dynamic_body> &response);
	void show_summary(bhttp::response<bhttp::dynamic_body> &response);
	void show_system(bhttp::response<bhttp::dynamic_body> &response);
	void show_uris(bhttp::response<bhttp::dynamic_body> &response);
	void show_pcapfile(bhttp::response<bhttp::dynamic_body> &response);
	void show_network_flow(bhttp::response<bhttp::dynamic_body> &response);
	void show_current_packet(bhttp::response<bhttp::dynamic_body> &response);
#if defined(PYTHON_BINDING)
	void show_python_script(bhttp::response<bhttp::dynamic_body> &response);
	void show_python_globals(bhttp::response<bhttp::dynamic_body> &response);
#endif

	void handle_post_message(bhttp::response<bhttp::dynamic_body> &response);
	void upload_pcapfile(bhttp::response<bhttp::dynamic_body> &response);
#if defined(PYTHON_BINDING)
	void upload_python_script(bhttp::response<bhttp::dynamic_body> &response);
#endif

	void handle_put_message(bhttp::response<bhttp::dynamic_body> &response);
	void update_network_flow(bhttp::response<bhttp::dynamic_body> &response);

	// Variables
	bhttp::request<bhttp::dynamic_body> request;
        send_lambda send_response;
        std::string ipsrc = "";
        int32_t total_requests_ = 0;
        int32_t total_responses_ = 0;
        int64_t total_bytes_ = 0;
        std::time_t last_time_ = (std::time_t)0;
        tcp::socket socket_;
        boost::beast::flat_buffer buffer_;
        std::shared_ptr<void> res_;
	SharedPointer<NetworkStack> stack_ = nullptr;
	PacketDispatcher *pdis_ = nullptr;
};

/* LCOV_EXCL_STOP */

} // namespace aiengine

#endif // SRC_HTTPSESSION_H_
