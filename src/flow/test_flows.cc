/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include <string>
#include <random>
#include "FlowCache.h"
#include "FlowManager.h"
#include "IPAddress.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE flowtest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_AUTO_TEST_SUITE (flowcache_test_suite_static)

BOOST_AUTO_TEST_CASE (test01)
{
	FlowCache *fc = new FlowCache();
	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 0);
	BOOST_CHECK(fc->getTotalFails() == 0);

	fc->createFlows(1000);
	BOOST_CHECK(fc->getTotalFlows() == 1000);
	fc->destroyFlows(10000);
	BOOST_CHECK(fc->getTotalFlows() == 0);
	delete fc;
}

BOOST_AUTO_TEST_CASE (test02)
{
        FlowCache *fc = new FlowCache();
        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

        fc->createFlows(2);

        BOOST_CHECK(fc->getTotalFlows() == 2);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	auto f1 = fc->acquireFlow();
	auto f2 = fc->acquireFlow();
	auto f3 = fc->acquireFlow();

        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 2);
        BOOST_CHECK(fc->getTotalFails() == 1);
	BOOST_CHECK(fc->getCurrentUseMemory() == (2 * FlowCache::flowSize));
	BOOST_CHECK(f1 !=  nullptr);
	BOOST_CHECK(f2 !=  nullptr);
	BOOST_CHECK(f3 ==  nullptr);

	fc->releaseFlow(f2);
	fc->releaseFlow(f1);

	BOOST_CHECK(fc->getTotalFlows() == 2);
        BOOST_CHECK(fc->getTotalReleases() == 2);

	BOOST_CHECK(fc->getTotalAcquires() == 2);
        BOOST_CHECK(fc->getTotalFails() == 1);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	delete fc;
}

BOOST_AUTO_TEST_CASE (test03)
{
        FlowCache *fc = new FlowCache();

	auto f1 = SharedPointer<Flow>(new Flow());
	auto f2 = SharedPointer<Flow>(new Flow());
	auto f3 = SharedPointer<Flow>(new Flow());

        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	fc->releaseFlow(f1);

	BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(fc->getTotalFlows() == 1);
        BOOST_CHECK(fc->getTotalReleases() == 1);
        BOOST_CHECK(fc->getTotalAcquires() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);

	auto f4 = fc->acquireFlow();

	BOOST_CHECK(f1 == f4);

	delete fc;
}

BOOST_AUTO_TEST_CASE (test04)
{
	FlowCache *fc = new FlowCache();
	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 0);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	fc->createFlows(10);
	BOOST_CHECK(fc->getTotalFlows() == 10);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 0);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	fc->destroyFlows(9);

	BOOST_CHECK(fc->getTotalFlows() == 1);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	fc->destroyFlows(9);

	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 0);

	fc->createFlows(1);

	BOOST_CHECK(fc->getTotalFlows() == 1);

	auto f1 = fc->acquireFlow();

	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 1);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == FlowCache::flowSize);

	auto f2 = fc->acquireFlow();

	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 1);
	BOOST_CHECK(fc->getTotalFails() == 1);
	BOOST_CHECK(fc->getCurrentUseMemory() == FlowCache::flowSize);
	BOOST_CHECK(f2 == nullptr);

	fc->releaseFlow(f1);
	fc->destroyFlows(1);
	delete fc;
}

BOOST_AUTO_TEST_CASE (test05)
{
        FlowCache *fc = new FlowCache();
        fc->createFlows(10);

	auto f1 = fc->acquireFlow();
	auto f2 = fc->acquireFlow();
	auto f3 = fc->acquireFlow();

	BOOST_CHECK(fc->getCurrentUseMemory() == 3 * FlowCache::flowSize);
	BOOST_CHECK(fc->getTotalFlows() == 7);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 3);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(f2 != f1);
	BOOST_CHECK(f1 != f3);

	fc->releaseFlow(f1);
	fc->releaseFlow(f2);
	fc->releaseFlow(f3);
	BOOST_CHECK(fc->getTotalReleases() == 3);
	BOOST_CHECK(fc->getTotalFlows() == 10);

	fc->destroyFlows(fc->getTotalFlows());
	delete fc;
}

BOOST_AUTO_TEST_CASE (test06)
{
        FlowCache *fc = new FlowCache();
        fc->createFlows(1);

        auto f1 = fc->acquireFlow();

	BOOST_CHECK(fc->getTotalFlows() == 0);

	f1->setId(10);
	f1->total_bytes[0] = 10;
	f1->total_packets[0] = 10;

        fc->releaseFlow(f1);
	BOOST_CHECK(fc->getTotalFlows() == 1);
        BOOST_CHECK(fc->getTotalReleases() == 1);

	auto f2 = fc->acquireFlow();
        fc->destroyFlows(fc->getTotalFlows());
        delete fc;
}

BOOST_AUTO_TEST_CASE (test07)
{
        FlowCache *fc = new FlowCache();
        fc->createFlows(1);

        auto f1 = fc->acquireFlow();

	f1->setFiveTuple(inet_addr("192.168.1.1"), 2345, IPPROTO_TCP, inet_addr("54.12.5.1"), 80);

	Json j;
	f1->show(j);

	BOOST_CHECK(j["upstream"]["bytes"] == 0);
	BOOST_CHECK(j["downstream"]["bytes"] == 0);
	BOOST_CHECK(j["upstream"]["ttl"] == 0);
	BOOST_CHECK(j["downstream"]["ttl"] == 0);
	BOOST_CHECK(j["upstream"]["packets"] == 0);
	BOOST_CHECK(j["downstream"]["packets"] == 0);
	BOOST_CHECK(j["ip"]["dst"] == "54.12.5.1");
	BOOST_CHECK(j["ip"]["src"] == "192.168.1.1");
	BOOST_CHECK(j["port"]["dst"] == 80);
	BOOST_CHECK(j["port"]["src"] == 2345);

        fc->releaseFlow(f1);
        BOOST_CHECK(fc->getTotalFlows() == 1);
        BOOST_CHECK(fc->getTotalReleases() == 1);

        fc->destroyFlows(fc->getTotalFlows());
        delete fc;
}

BOOST_AUTO_TEST_CASE (test08) // Simulate bad_alloc on the system
{
        FlowCache *fc = new FlowCache();

	fc->setDynamicAllocatedMemory(true);

	// This flag will generate a exception on the allocations check Cache_Imp.h
	fc->setGenerateBadAllocException(true);

	fc->createFlows(1);
	BOOST_CHECK(fc->getTotalFlows() == 0);

	auto flow = fc->acquireFlow();
	BOOST_CHECK(flow == nullptr);

	fc->setGenerateBadAllocException(false);
	delete fc;
}

BOOST_AUTO_TEST_SUITE_END( )

BOOST_AUTO_TEST_SUITE (flowcache_dynamic)

BOOST_AUTO_TEST_CASE (test01)
{
        FlowCache *fc = new FlowCache();

	fc->setDynamicAllocatedMemory(true);
	BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

        fc->createFlows(2);

        BOOST_CHECK(fc->getTotalFlows() == 2);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	auto f1 = fc->acquireFlow();
	auto f2 = fc->acquireFlow();
	auto f3 = fc->acquireFlow();

        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 3);
        BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == (3 * FlowCache::flowSize));
	BOOST_CHECK(f1 !=  nullptr);
	BOOST_CHECK(f2 !=  nullptr);
	BOOST_CHECK(f3 !=  nullptr);

	fc->releaseFlow(f2);
	fc->releaseFlow(f1);

	BOOST_CHECK(fc->getTotalFlows() == 2);
        BOOST_CHECK(fc->getTotalReleases() == 2);

	BOOST_CHECK(fc->getTotalAcquires() == 3);
        BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == FlowCache::flowSize);

	delete fc;
}

BOOST_AUTO_TEST_CASE (test02)
{
	FlowCache *fc = new FlowCache();
	fc->setDynamicAllocatedMemory(true);
	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 0);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	fc->createFlows(10);
	BOOST_CHECK(fc->getTotalFlows() == 10);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 0);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	fc->destroyFlows(9);

	BOOST_CHECK(fc->getTotalFlows() == 1);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);

	fc->destroyFlows(9);

	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 0);

	fc->createFlows(1);

	BOOST_CHECK(fc->getTotalFlows() == 1);

	auto f1 = fc->acquireFlow();

	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 1);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == FlowCache::flowSize);

	auto f2 = fc->acquireFlow();

	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 2);
	BOOST_CHECK(fc->getTotalFails() == 0);
	BOOST_CHECK(fc->getCurrentUseMemory() == FlowCache::flowSize * 2);
	BOOST_CHECK(f2 != nullptr);

	fc->releaseFlow(f1);
	fc->destroyFlows(1);
	delete fc;
}

BOOST_AUTO_TEST_SUITE_END( )

BOOST_AUTO_TEST_SUITE (flowmanager) // name of the test suite is stringtest

BOOST_AUTO_TEST_CASE (test01)
{
        FlowManager *fm = new FlowManager();
        auto f1 = SharedPointer<Flow>(new Flow());
        auto f2 = SharedPointer<Flow>(new Flow());
	IPAddress addr;

	addr.setSourceAddress(inet_addr("192.168.1.1"));
	addr.setDestinationAddress(inet_addr("192.168.1.255"));

        unsigned long h1 = addr.getHash(2, IPPROTO_UDP, 5);
        unsigned long h2 = addr.getHash(5, IPPROTO_UDP, 2);
        unsigned long hfail = addr.getHash(10, IPPROTO_UDP, 10); // for fails

	f2->setId(h1);
        f1->setId(hfail);
        fm->addFlow(f1);
	fm->addFlow(f2);

	// fm->showFlows(std::cout, 10);
        BOOST_CHECK(fm->getTotalFlows() == 2);

	int items = 0;
	for (auto &item: fm->getFlowTable())
		++items;

	BOOST_CHECK(items == 2);

	BOOST_CHECK(fm->getAllocatedMemory() == 2 * (FlowCache::flowSize + sizeof(FlowNode)));
	fm->removeFlow(f2);
	BOOST_CHECK(fm->getAllocatedMemory() == FlowCache::flowSize + sizeof(FlowNode));

        delete fm;
}

// Test the lookups of the flows
BOOST_AUTO_TEST_CASE (test02)
{
	FlowManager *fm = new FlowManager();
	auto f1 = SharedPointer<Flow>(new Flow());

	IPAddress addr;

	addr.setSourceAddress(inet_addr("192.168.1.1"));
	addr.setDestinationAddress(inet_addr("192.168.1.255"));

        unsigned long h1 = addr.getHash(137, IPPROTO_UDP, 137);
        unsigned long h2 = addr.getHash(137, IPPROTO_UDP, 137);
        unsigned long hfail = addr.getHash(10, IPPROTO_UDP, 10); // for fails

	f1->setId(h1);
	fm->addFlow(f1);
	BOOST_CHECK(fm->getTotalFlows() == 1);

	auto f2 = fm->findFlow(hfail, h2);
	BOOST_CHECK(f2 != nullptr);
	BOOST_CHECK(f1 == f2);
	BOOST_CHECK(f1.get() == f2.get());

	auto f3 = fm->findFlow(hfail, hfail);
	BOOST_CHECK(f3 == nullptr);
	BOOST_CHECK(fm->getTotalFlows() == 1);

	delete fm;
}

// Test lookups
BOOST_AUTO_TEST_CASE (test03)
{
        FlowManager *fm = new FlowManager();
        auto f1 = SharedPointer<Flow>(new Flow());

        IPAddress addr1;

        addr1.setSourceAddress(inet_addr("192.168.1.1"));
        addr1.setDestinationAddress(inet_addr("192.168.1.2"));

        unsigned long h1 = addr1.getHash(137, IPPROTO_UDP, 137);
        unsigned long h2 = addr1.getHash(137, IPPROTO_UDP, 137);

        f1->setId(h1);
        fm->addFlow(f1);
        BOOST_CHECK(fm->getTotalFlows() == 1);

	// The same conversation but other direction
        IPAddress addr2;

	// 5tuple 192.168.1.2:137:17:192.168.1.1:137
        addr2.setSourceAddress(inet_addr("192.168.1.2"));
        addr2.setDestinationAddress(inet_addr("192.168.1.1"));

        h1 = addr1.getHash(137, IPPROTO_UDP, 137);
        h2 = addr1.getHash(137, IPPROTO_UDP, 137);

        auto f2 = fm->findFlow(h1, h2);
        BOOST_CHECK(f1 == f2);
        BOOST_CHECK(f1.get() == f2.get());

	// Different conversation, different port same ips

	// 5tuple 192.168.1.2:138:17:192.168.1.1:138
        h1 = addr1.getHash(138, IPPROTO_UDP, 138);
        h2 = addr1.getHash(138, IPPROTO_UDP, 138);

        auto f3 = fm->findFlow(h1, h2);
        BOOST_CHECK(f3 != f2);
        BOOST_CHECK(f3 == nullptr) ;

        delete fm;
}

// Test lookups
BOOST_AUTO_TEST_CASE (test04)
{
        FlowManager *fm = new FlowManager();
        auto f1 = SharedPointer<Flow>(new Flow());

        IPAddress addr1;

	// Inject 5tuple 83.156.1.2:800:6:172.100.31.196:80
        uint32_t ipsrc = inet_addr("83.156.1.2");
        uint32_t ipdst = inet_addr("172.100.31.196");
        addr1.setSourceAddress(ipsrc);
        addr1.setDestinationAddress(ipdst);

        uint16_t portsrc = 800;
        uint16_t portdst = 80;
        uint16_t proto = IPPROTO_TCP;

        unsigned long h1 = addr1.getHash(portsrc, proto, portdst);
        unsigned long h2 = addr1.getHash(portdst, proto, portsrc);

        f1->setId(h1);
        fm->addFlow(f1);

	// Inject 5tuple 172.100.31.196:80:6:83.156.1.2:800
        IPAddress addr2;

        addr2.setSourceAddress(ipdst);
        addr2.setDestinationAddress(ipsrc);

        h1 = addr2.getHash(portdst, proto, portsrc);
        h2 = addr2.getHash(portsrc, proto, portdst);

        auto f2 = fm->findFlow(h1, h2);
        BOOST_CHECK(f1 == f2);

	delete fm;
}

// Test lookups and removes
BOOST_AUTO_TEST_CASE (test05)
{
        FlowManager *fm = new FlowManager();
        auto f1 = SharedPointer<Flow>(new Flow());

        IPAddress addr;

        addr.setSourceAddress(inet_addr("192.156.1.2"));
        addr.setDestinationAddress(inet_addr("10.10.1.1"));

        unsigned long h1 = addr.getHash(137, IPPROTO_UDP, 5000);
        unsigned long h2 = addr.getHash(5000, IPPROTO_UDP, 137);
        unsigned long hfail = 10^10^10^10^10; // for fails

	f1->setFiveTuple(inet_addr("192.156.1.2"), 137, IPPROTO_UDP, inet_addr("10.10.1.1"), 5000);
        f1->setId(h1);

	BOOST_CHECK(f1.use_count() == 1);
	fm->addFlow(f1);
	BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(fm->getTotalFlows() == 1);

        auto f2 = fm->findFlow(hfail, h2);
       	BOOST_CHECK(f2 == nullptr);

        f2 = fm->findFlow(h1, hfail);
       	BOOST_CHECK(f2 != nullptr);

       	auto f3 = fm->findFlow("192.156.1.2", 137, IPPROTO_TCP, "", 5000);
       	BOOST_CHECK(f3 == nullptr);

       	f3 = fm->findFlow("192.156.1.2", 137, IPPROTO_TCP, "10.10.1.2", 5000);
       	BOOST_CHECK(f3 == nullptr);

       	f3 = fm->findFlow("192.156.1.2", 137, IPPROTO_UDP, "10.10.1.2", 5000);
       	BOOST_CHECK(f3 == nullptr);

       	f3 = fm->findFlow("192.156.1.2", 137, IPPROTO_UDP, "10.10.1.1", 5000);
       	BOOST_CHECK(f3 != nullptr);

       	BOOST_CHECK(f1 == f2);
       	BOOST_CHECK(f2 == f3);

       	f3 = fm->findFlow("192.156.1.2", 137, IPPROTO_UDP, "10.10.1.0", 5000);
       	BOOST_CHECK(f3 == nullptr);

	fm->removeFlow(f1);
        BOOST_CHECK(fm->getTotalFlows() == 0);

        delete fm;
}


// Test some IPv6 addresses
BOOST_AUTO_TEST_CASE (test06)
{
	FlowManager *fm = new FlowManager();
        auto f1 = SharedPointer<Flow>(new Flow());

        IPAddress addr1, addr2;
       	struct in6_addr src6;
       	struct in6_addr dst6;

       	inet_pton(AF_INET6, "2001:db8::1", &src6);
       	inet_pton(AF_INET6, "2007:1294:db8::16", &dst6);

        addr1.setSourceAddress6(&src6);
        addr1.setDestinationAddress6(&dst6);

        unsigned long h1 = addr1.getHash(137, IPPROTO_UDP, 15007);
        unsigned long h2 = addr1.getHash(15007, IPPROTO_UDP, 137);

       	f1->setId(h1);
        fm->addFlow(f1);

       	BOOST_CHECK(f1.use_count() == 2);
       	BOOST_CHECK(fm->getTotalFlows() == 1);

        inet_pton(AF_INET6, "2001:db8::1", &dst6);
        inet_pton(AF_INET6, "2007:1294:db8::16", &src6);

        addr2.setSourceAddress6(&src6);
        addr2.setDestinationAddress6(&dst6);

        h2 = addr2.getHash(137, IPPROTO_UDP, 15007);
        h1 = addr2.getHash(15007, IPPROTO_UDP, 137);

       	auto f2 = fm->findFlow(h1, h2);
       	BOOST_CHECK(f1 == f2);

       	auto f3 = fm->findFlow("", 137, IPPROTO_TCP, "", 15007);
       	BOOST_CHECK(f3 == nullptr);

       	f3 = fm->findFlow("", 137, IPPROTO_TCP, "2007:1294:db8::16", 15007);
       	BOOST_CHECK(f3 == nullptr);

       	f3 = fm->findFlow("2001:db8::1", 137, IPPROTO_TCP, "2007:1294:db8::16", 15007);
       	BOOST_CHECK(f3 == nullptr);

       	f3 = fm->findFlow("2001:db8::1", 137, IPPROTO_UDP, "2007:1294:db8::16", 15007);
       	BOOST_CHECK(f3 != nullptr);
       	BOOST_CHECK(f3 == f1);

       	// Change the destination port
       	f3 = fm->findFlow("2001:db8::1", 137, IPPROTO_UDP, "2007:1294:db8::16", 15008);
       	BOOST_CHECK(f3 == nullptr);

       	f3 = fm->findFlow("2007:1294:db8::16", 15007, IPPROTO_UDP, "2001:db8::1", 137);
       	BOOST_CHECK(f3 != nullptr);
       	BOOST_CHECK(f3 == f1);

       	delete fm;
}

BOOST_AUTO_TEST_SUITE_END( )

BOOST_AUTO_TEST_SUITE (flowcache_static_and_flowmanager)

BOOST_AUTO_TEST_CASE (test01)
{
	FlowCache *fc = new FlowCache();
	FlowManager *fm = new FlowManager();

        IPAddress addr;

        addr.setSourceAddress(inet_addr("192.156.1.2"));
        addr.setDestinationAddress(inet_addr("10.10.1.1"));

        unsigned long h1 = addr.getHash(137, IPPROTO_UDP, 5000);
        unsigned long h2 = addr.getHash(137, IPPROTO_UDP, 5000);
        unsigned long hfail = addr.getHash(138, IPPROTO_UDP, 5000); // for fails

	fc->createFlows(10);
	auto f1 = fc->acquireFlow();
	auto f2 = fc->acquireFlow();
	auto f3 = fc->acquireFlow();
	auto f4 = fc->acquireFlow();

	BOOST_CHECK(f1.use_count() == 1); // one is the cache and the other f1
        BOOST_CHECK(fm->getTotalFlows() == 0);

	f1->setId(h1);
	f2->setId(0xaabbccdd);
	f3->setId(0xaabbccFF);
	f4->setId(0x00bbccFF);

	fm->addFlow(f1);
        BOOST_CHECK(fm->getTotalFlows() == 1);
	auto ff = fm->findFlow(h1, hfail);
	BOOST_CHECK(ff.get() == f1.get());

	fm->addFlow(f2);
        BOOST_CHECK(fm->getTotalFlows() == 2);

	fm->addFlow(f3);
        BOOST_CHECK(fm->getTotalFlows() == 3);
	fm->addFlow(f4);

	fm->removeFlow(f1);
        fm->removeFlow(f2);
        fm->removeFlow(f3);
        BOOST_CHECK(fm->getTotalFlows() == 1);

        fm->removeFlow(f4);
	// TOOD remove flow

	delete fm;
	delete fc;
}

BOOST_AUTO_TEST_CASE (test02)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
	std::vector<SharedPointer<Flow>> v;
	int max_flows = 4;

        fc->createFlows(max_flows);

	for (int i = 0; i < max_flows + 2; ++i) {
        	auto f1 = fc->acquireFlow();
		if (f1) {
        		IPAddress addr;

        		addr.setSourceAddress(inet_addr("192.156.1.2"));
        		addr.setDestinationAddress(inet_addr("10.10.1.1"));

        		unsigned long h1 = addr.getHash(137, IPPROTO_TCP, i);
        		unsigned long h2 = addr.getHash(i, IPPROTO_TCP, 137);

        		f1->setId(h1);

        		fm->addFlow(f1);
        		BOOST_CHECK(fm->getTotalFlows() == i + 1);
		}
	}

	BOOST_CHECK(fm->getTotalFlows() == max_flows);
	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == max_flows);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalFails() == 2);

	for (int i = 0; i < max_flows; ++i) {
        	IPAddress addr;

        	addr.setSourceAddress(inet_addr("192.156.1.2"));
        	addr.setDestinationAddress(inet_addr("10.10.1.1"));

        	unsigned long h1 = addr.getHash(137, IPPROTO_TCP, i);
        	unsigned long h2 = addr.getHash(i, IPPROTO_TCP, 137);

		auto f1 = fm->findFlow(h1, h2);
		if (f1) {
			fm->removeFlow(f1);
			v.push_back(f1);
		}
	}

	BOOST_CHECK(fm->getTotalFlows() == 0);

	for (auto value: v) {
		fc->releaseFlow(value);
	}

	BOOST_CHECK(fc->getTotalReleases() == max_flows);
}

BOOST_AUTO_TEST_CASE (test03)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        std::vector<SharedPointer<Flow>> v;

        fc->createFlows(254);

        for (int i = 0; i < 254; ++i) {
                auto f1 = fc->acquireFlow();
                if (f1) {
        		IPAddress addr;

			uint32_t ipsrc = inet_addr("83.156.1.2");
			uint32_t ipdst = inet_addr("172.100.31.196");
        		addr.setSourceAddress(ipsrc);
        		addr.setDestinationAddress(ipdst);

			uint16_t portsrc = 800 + i;
			uint16_t portdst = 80;
			uint16_t proto = IPPROTO_TCP;

	      		unsigned long h1 = addr.getHash(portsrc, proto, portdst);
        		unsigned long h2 = addr.getHash(portdst, proto, portsrc);

                        f1->setId(h1);
			f1->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

                        fm->addFlow(f1);
			f1->total_packets[0] = 1;
                        BOOST_CHECK(fm->getTotalFlows() == i + 1);
                }
        }

        BOOST_CHECK(fm->getTotalFlows() == 254);
        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 254);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);

	// Now the second packet of the flow arrives
        for (int i = 0; i < 254; ++i) {
        	IPAddress addr;
		uint32_t ipsrc = inet_addr("172.100.31.196");
                uint32_t ipdst = inet_addr("83.156.1.2");

        	addr.setSourceAddress(ipsrc);
        	addr.setDestinationAddress(ipdst);

                uint16_t portsrc = 80;
                uint16_t portdst = 800 + i;
                uint16_t proto = IPPROTO_TCP;

	      	unsigned long h1 = addr.getHash(portsrc, proto, portdst);
        	unsigned long h2 = addr.getHash(portdst, proto, portsrc);

		auto f1 = fm->findFlow(h1, h2);
		BOOST_CHECK(f1 != nullptr);
		// The flow only have one packet
		BOOST_CHECK(f1->total_packets[0] == 1);
		++f1->total_packets[0];
        }
        BOOST_CHECK(fm->getTotalFlows() == 254);
        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 254);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);


	Json jf;
	std::ostringstream out;

	fm->showFlows(jf, 500);
	fm->showFlows(jf, 1000, "tcp");

	fm->showFlows(out, 500);
	fm->showFlows(out, 1000, "tcp");
}

BOOST_AUTO_TEST_CASE (test04)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        std::vector<SharedPointer<Flow>> v;

        fc->createFlows(254);

        for (int i = 0; i < 254; ++i) {
                auto f1 = fc->acquireFlow();

                if (f1) {
			std::ostringstream os;
			IPAddress addr;

			os << "10.253." << i << "1";
			std::string ipsrc_str = "192.168.1.1";
			uint32_t ipsrc = inet_addr(ipsrc_str.c_str());
                        uint32_t ipdst = inet_addr(os.str().c_str());
                        uint16_t portsrc = 1200 + i;
                        uint16_t portdst = 8080;
                        uint16_t proto = IPPROTO_TCP;

        		addr.setSourceAddress(ipsrc);
        		addr.setDestinationAddress(ipdst);

	      		unsigned long h1 = addr.getHash(portsrc, proto, portdst);
        		unsigned long h2 = addr.getHash(portdst, proto, portsrc);

                        f1->setId(h1);
                        f1->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

                        fm->addFlow(f1);
                        f1->total_packets[0] = 1;
                        BOOST_CHECK(fm->getTotalFlows() == i+1);
                }
        }
        // Now the second packet of the flow arrives
        for (int i = 0; i < 254; ++i) {
		std::ostringstream os;
		IPAddress addr;

                os << "10.253." << i << "1";
                std::string ipsrc_str = "192.168.1.1";
                uint32_t ipdst = inet_addr(ipsrc_str.c_str());
                uint32_t ipsrc = inet_addr(os.str().c_str());
                uint16_t portdst = 1200 + i;
                uint16_t portsrc = 8080;
                uint16_t proto = IPPROTO_TCP;

        	addr.setSourceAddress(ipdst);
        	addr.setDestinationAddress(ipsrc);

	      	unsigned long h1 = addr.getHash(portsrc, proto, portdst);
        	unsigned long h2 = addr.getHash(portdst, proto, portsrc);

                auto f1 = fm->findFlow(h1, h2);
		BOOST_CHECK(f1 != nullptr);
                // The flow only have one packet
                BOOST_CHECK(f1->total_packets[0] == 1);
                ++f1->total_packets[0];
        }
}

BOOST_AUTO_TEST_CASE (test05)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());

	fm->setFlowCache(fc);

        fc->createFlows(4);
        for (int i = 0; i < 4; ++i) {
                auto f = fc->acquireFlow();
                if (f) {
			IPAddress addr;
                        std::ostringstream os;

                        os << "10.253." << i << "1";
                        std::string ipsrc_str = "192.168.1.1";

			uint32_t ipsrc = inet_addr(ipsrc_str.c_str());
                        uint32_t ipdst = inet_addr(os.str().c_str());

        		addr.setSourceAddress(ipdst);
        		addr.setDestinationAddress(ipsrc);

                        uint16_t portsrc = 1200 + i;
                        uint16_t portdst = 8080;
                        uint16_t proto = IPPROTO_TCP;

	      		unsigned long h1 = addr.getHash(portsrc, proto, portdst);
        		unsigned long h2 = addr.getHash(portdst, proto, portsrc);

                        f->setId(h1);
                        f->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

                        fm->addFlow(f);
                        f->total_packets[0] = 1;
                        BOOST_CHECK(fm->getTotalFlows() == i + 1);
                }
        }
        BOOST_CHECK(fm->getTotalFlows() == 4);
        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 4);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);

	// the user from cmd execute the command
	fm->flush();

	BOOST_CHECK(fm->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalFlows() == 4);
        BOOST_CHECK(fc->getTotalAcquires() == 4);
        BOOST_CHECK(fc->getTotalReleases() == 4);
        BOOST_CHECK(fc->getTotalFails() == 0);
}

// This test tries all the combination of ports that a tuple of ips can have in order to find hash problems
BOOST_AUTO_TEST_CASE (test06)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());

        fm->setFlowCache(fc);
        fc->createFlows(65536);

        for (int i = 1; i < 65536; ++i) {
                auto f = fc->acquireFlow();
                if (f) {
                        IPAddress addr;
                        std::ostringstream os;

                        os << "10.253." << i << "1";
                        std::string ipsrc_str = "192.168.1.1";

                        uint32_t ipsrc = inet_addr(ipsrc_str.c_str());
                        uint32_t ipdst = inet_addr(os.str().c_str());

                        addr.setSourceAddress(ipdst);
                        addr.setDestinationAddress(ipsrc);

                        uint16_t portsrc = i;
                        uint16_t portdst = 80;
                        uint16_t proto = IPPROTO_TCP;

                        unsigned long h1 = addr.getHash(portsrc, proto, portdst);
                        unsigned long h2 = addr.getHash(portdst, proto, portsrc);

                        f->setId(h1);
                        f->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

                        fm->addFlow(f);
                        f->total_packets[0] = 1;
                        BOOST_CHECK(fm->getTotalFlows() == i);
                }
        }
	BOOST_CHECK(fm->getTotalFlows() == 65536 - 1);
        BOOST_CHECK(fc->getTotalFlows() == 1);
        BOOST_CHECK(fc->getTotalAcquires() == 65536 - 1);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalFails() == 0);
}

BOOST_AUTO_TEST_SUITE_END( )

BOOST_AUTO_TEST_SUITE (flowcache_dynamic_and_flowmanager)

BOOST_AUTO_TEST_CASE (test01)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
	std::vector<SharedPointer<Flow>> v;

	fc->setDynamicAllocatedMemory(true);

        fc->createFlows(64);

	for (int i = 0; i < 66; ++i) {
        	auto f1 = fc->acquireFlow();
		if (f1) {
        		IPAddress addr;

        		addr.setSourceAddress(inet_addr("192.156.1.2"));
        		addr.setDestinationAddress(inet_addr("10.10.1.1"));

        		unsigned long h1 = addr.getHash(137, IPPROTO_TCP, i);
        		unsigned long h2 = addr.getHash(i, IPPROTO_TCP, 137);

        		f1->setId(h1);

        		fm->addFlow(f1);
        		BOOST_CHECK(fm->getTotalFlows() == i+1);
		}
	}

	BOOST_CHECK(fm->getTotalFlows() == 66);
	BOOST_CHECK(fc->getTotalFlows() == 0);
	BOOST_CHECK(fc->getTotalAcquires() == 66);
	BOOST_CHECK(fc->getTotalReleases() == 0);
	BOOST_CHECK(fc->getTotalFails() == 0);

	for (int i = 0; i < 64; ++i) {
        	IPAddress addr;

        	addr.setSourceAddress(inet_addr("192.156.1.2"));
        	addr.setDestinationAddress(inet_addr("10.10.1.1"));

        	unsigned long h1 = addr.getHash(137, IPPROTO_TCP, i);
        	unsigned long h2 = addr.getHash(i, IPPROTO_TCP, 137);

		auto f1 = fm->findFlow(h1, h2);
		if (f1) {
			fm->removeFlow(f1);
			v.push_back(f1);
		}
	}

	BOOST_CHECK(fm->getTotalFlows() == 2);

	for (auto value: v) {
		fc->releaseFlow(value);
	}

	BOOST_CHECK(fc->getTotalReleases() == 64);
}

// Verify the order of non use flows
BOOST_AUTO_TEST_CASE (test02)
{       
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        std::vector<SharedPointer<Flow>> v;
       
	fm->setFlowCache(fc); 
        fc->createFlows(16);

	// There is nothing there
	auto item = fm->getFlowTable().begin();
	BOOST_CHECK(item == nullptr);
 
        for (int i = 0; i < 16; ++i) {
                auto flow = fc->acquireFlow();
                if (flow) {
			IPAddress addr;
			uint32_t ipdst = inet_addr("10.10.1.1");
                	std::ostringstream out;

			out << "192.168.0." << i;
			auto ipsrc = inet_addr(out.str().c_str());
			addr.setSourceAddress(ipsrc);
			addr.setDestinationAddress(ipdst);
		
			unsigned long h1 = addr.getHash(137, IPPROTO_TCP, 100);
			unsigned long h2 = addr.getHash(100, IPPROTO_TCP, 137);
		
			flow->setId(h1);
			flow->setFiveTuple(ipsrc, 100, IPPROTO_TCP, ipdst, 137);	
			fm->addFlow(flow);
		}
	}
	BOOST_CHECK(fm->getTotalFlows() == 16);

	// Now generates lookups

	auto flow = fm->findFlow("192.168.0.5", 100, IPPROTO_TCP, "10.10.1.1", 137);
	BOOST_CHECK(flow != nullptr);
	flow->setLastPacketTime(1);

	flow = fm->findFlow("192.168.0.0", 100, IPPROTO_TCP, "10.10.1.1", 137);
	BOOST_CHECK(flow != nullptr);
	flow->setLastPacketTime(2);

	flow = fm->findFlow("192.168.0.5", 100, IPPROTO_TCP, "10.10.1.1", 137);
	BOOST_CHECK(flow != nullptr);
	flow->setLastPacketTime(3);

	int total_flows = 0;
	for (auto &item: fm->getFlowTable())
		++total_flows;

	BOOST_CHECK(total_flows == 16);

	auto first_flow = *fm->getFlowTable().begin();
	BOOST_CHECK(first_flow != nullptr);
	BOOST_CHECK(flow == first_flow);
}

// Verify the order of flows with random access 
BOOST_AUTO_TEST_CASE (test03)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());

        fm->setFlowCache(fc);
        fc->createFlows(16);

        for (int i = 0; i < 16; ++i) {
                auto flow = fc->acquireFlow();
                if (flow) {
                        std::ostringstream out;

                        out << "192.168.0." << i;
                        auto ipsrc = inet_addr(out.str().c_str());

                        flow->setFiveTuple(ipsrc, 0, IPPROTO_TCP, 0, 0);
                        flow->setId(i);
                        fm->addFlow(flow);
                }
        }
	std::random_device rd; 
    	std::mt19937 gen(rd()); // seed the generator
    	std::uniform_int_distribution<> distr(0, 16 - 1); 

	for(int n = 0; n < 40; ++n) {
		auto flow = fm->findFlow(distr(gen), 1000000);
		flow->setLastPacketTime(n + 1);
	}
	int max_value = 1000;
	for (auto &flow: fm->getFlowTable()) {
		BOOST_CHECK(max_value >= flow->getLastPacketTime());
		max_value = flow->getLastPacketTime();
	}	
}

// Verify the all flows timeout correctly
BOOST_AUTO_TEST_CASE (test04)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
	int max_flows = 4;

        fm->setFlowCache(fc);
        fc->createFlows(max_flows);
        
        for (int i = 0; i < max_flows; ++i) {
                auto flow = fc->acquireFlow();
                if (flow) {
                        std::ostringstream out;

                        out << "192.168.0." << i;
                        auto ipsrc = inet_addr(out.str().c_str());

                        flow->setFiveTuple(ipsrc, 0, IPPROTO_TCP, 0, 0);
                        flow->setId(i);
                        fm->addFlow(flow);
                }
        }

        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == max_flows);
        BOOST_CHECK(fc->getTotalFails() == 0);

        BOOST_CHECK(fm->getTotalFlows() == max_flows);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

        for (auto &flow: fm->getFlowTable())
                flow->setLastPacketTime(1);

	fm->updateTimers(200);

        BOOST_CHECK(fm->getTotalFlows() == 0);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == max_flows);

        BOOST_CHECK(fc->getTotalFlows() == max_flows);
        BOOST_CHECK(fc->getTotalReleases() == max_flows);
        BOOST_CHECK(fc->getTotalAcquires() == max_flows);
        BOOST_CHECK(fc->getTotalFails() == 0);
}

// Verify that the method purge works correctly
BOOST_AUTO_TEST_CASE (test05)
{
        FlowCachePtr fc = FlowCachePtr(new FlowCache());
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        int max_flows = 4;

        fm->setFlowCache(fc);
        fc->createFlows(8);

        for (int i = 0; i < 4; ++i) {
                auto flow = fc->acquireFlow();
                if (flow) {
                        std::ostringstream out;

                        out << "192.168.0." << i;
                        auto ipsrc = inet_addr(out.str().c_str());

                        flow->setFiveTuple(ipsrc, 0, IPPROTO_TCP, 0, 0);
                        flow->setId(i);
                        fm->addFlow(flow);
                }
        }
        for (int i = 0; i < 4; ++i) {
                auto flow = fc->acquireFlow();
                if (flow) {
                        std::ostringstream out;

                        out << "192.168.100." << i;
                        auto ipsrc = inet_addr(out.str().c_str());

                        flow->setFiveTuple(ipsrc, 0, IPPROTO_TCP, 0, 0);
                        flow->setId(i+100);
                        fm->addFlow(flow);
                	flow->setLastPacketTime(200);
                }
        }

        BOOST_CHECK(fm->getTotalFlows() == 8);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);
        BOOST_CHECK(fc->getTotalFlows() == 0);
        BOOST_CHECK(fc->getTotalReleases() == 0);
        BOOST_CHECK(fc->getTotalAcquires() == 8);
        BOOST_CHECK(fc->getTotalFails() == 0);

	fm->purge(201);

        BOOST_CHECK(fm->getTotalFlows() == 4);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 4);
        BOOST_CHECK(fc->getTotalFlows() == 4);
        BOOST_CHECK(fc->getTotalReleases() == 4);
        BOOST_CHECK(fc->getTotalAcquires() == 8);
        BOOST_CHECK(fc->getTotalFails() == 0);
}

BOOST_AUTO_TEST_SUITE_END( )

BOOST_AUTO_TEST_SUITE (flowmanager_time_release_flow) // test for manage the time with releasing the flows

BOOST_AUTO_TEST_CASE (test01)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow());
        auto f2 = SharedPointer<Flow>(new Flow());
	IPAddress addr;

	uint32_t ipsrc = inet_addr("1.1.1.1");
        uint32_t ipdst = inet_addr("213.200.11.87");
        uint16_t portsrc = 1000;
        uint16_t portdst = 80;
        uint16_t proto = IPPROTO_TCP;

       	addr.setSourceAddress(ipsrc);
       	addr.setDestinationAddress(ipdst);

	unsigned long h1 = addr.getHash(portsrc, proto, portdst);

        f1->setId(h1);
	f1->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

	// other flow
	ipsrc = inet_addr("2.2.2.2");
       	addr.setSourceAddress(ipsrc);
	unsigned long h2 = addr.getHash(portsrc, proto, portdst);

	f2->setId(h2);
	f2->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

        f1->setArriveTime(0);
        f2->setArriveTime(0);

        fm->addFlow(f1);
        fm->addFlow(f2);

	// fm->showFlowsByTime();

	f1->setLastPacketTime(100);
	f2->setLastPacketTime(1);

	BOOST_CHECK(f1.use_count() == 2);
	BOOST_CHECK(f2.use_count() == 2);

        BOOST_CHECK(fm->getTotalFlows() == 2);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	// This lookup generates the update of f1
	BOOST_CHECK(fm->findFlow(h1, 0) != nullptr);
	//fm->showFlowsByTime();

        // Update the time of the flows
        fm->updateTimers(200);

	// fm->showFlows(std::cout, 10);

        BOOST_CHECK(fm->getTotalFlows() == 1);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 1);
	BOOST_CHECK(f1.use_count() == 3);
	BOOST_CHECK(f2.use_count() == 1);
	BOOST_CHECK(f2.use_count() == 1);
}

BOOST_AUTO_TEST_CASE (test02)
{
  	FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
	auto f1 = SharedPointer<Flow>(new Flow(1));
	auto f2 = SharedPointer<Flow>(new Flow(2));
	auto f3 = SharedPointer<Flow>(new Flow(3));

	f1->setArriveTime(0);
	f2->setArriveTime(0);
	f3->setArriveTime(0);

	f1->setLastPacketTime(1);
	f2->setLastPacketTime(2);
	f3->setLastPacketTime(200);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);

	BOOST_CHECK(f1.use_count() == 2);
	BOOST_CHECK(f2.use_count() == 2);
	BOOST_CHECK(f3.use_count() == 2);

	BOOST_CHECK(fm->getTotalFlows() == 3);
	BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	// Update the time of the flows
	fm->updateTimers(200);

	BOOST_CHECK(fm->getTotalFlows() == 1);
	BOOST_CHECK(fm->getTotalTimeoutFlows() == 2);

	BOOST_CHECK(f1.use_count() == 1);
	BOOST_CHECK(f2.use_count() == 1);
	BOOST_CHECK(f3.use_count() == 2);
}

BOOST_AUTO_TEST_CASE (test03)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2));
        auto f3 = SharedPointer<Flow>(new Flow(3));
        auto f4 = SharedPointer<Flow>(new Flow(4));

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);
        f4->setArriveTime(0);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);
        fm->addFlow(f4);

        //fm->showFlowsByTime();

	f1->setLastPacketTime(1);
	f2->setLastPacketTime(200);
	f3->setLastPacketTime(2);
	f4->setLastPacketTime(210);

        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(f4.use_count() == 2);

        BOOST_CHECK(fm->getTotalFlows() == 4);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	BOOST_CHECK(fm->findFlow(2, 0));	
	BOOST_CHECK(fm->findFlow(4, 0));	

        // Update the time of the flows
	// Two flows will be removed due to the timeout f2 and f4
        fm->updateTimers(220);

        BOOST_CHECK(fm->getTotalFlows() == 2);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 2);
        BOOST_CHECK(f1.use_count() == 1);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 1);
        BOOST_CHECK(f4.use_count() == 3);
}

BOOST_AUTO_TEST_CASE (test04)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2));
        auto f3 = SharedPointer<Flow>(new Flow(3));
        auto f4 = SharedPointer<Flow>(new Flow(4));
        auto f5 = SharedPointer<Flow>(new Flow(5));

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);
        f4->setArriveTime(0);
        f5->setArriveTime(0);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);
        fm->addFlow(f4);
        fm->addFlow(f5);

	// The flows are not sorted on the multi_index
	f1->setLastPacketTime(150);
	f2->setLastPacketTime(110);
	f3->setLastPacketTime(12); // comatose flow
	f4->setLastPacketTime(17); // comatose flow
	f5->setLastPacketTime(140);

        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(f4.use_count() == 2);
        BOOST_CHECK(f5.use_count() == 2);

        BOOST_CHECK(fm->getTotalFlows() == 5);

	BOOST_CHECK(fm->findFlow(1, 0) != nullptr);
	BOOST_CHECK(fm->findFlow(2, 0) != nullptr);
	BOOST_CHECK(fm->findFlow(5, 0) != nullptr);

        fm->updateTimers(200);

	// fm->showFlows(std::cout, 10);

        BOOST_CHECK(fm->getTotalFlows() == 3);
        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 1);
        BOOST_CHECK(f4.use_count() == 1);
        BOOST_CHECK(f5.use_count() == 3); // last_flow_
}

BOOST_AUTO_TEST_CASE (test05)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
	FlowCachePtr fc = FlowCachePtr(new FlowCache());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(22));
        auto f3 = SharedPointer<Flow>(new Flow(3));
        auto f4 = SharedPointer<Flow>(new Flow(444));
        auto f5 = SharedPointer<Flow>(new Flow(7));

	fm->setFlowCache(fc);

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);
        f4->setArriveTime(0);
        f5->setArriveTime(0);

        BOOST_CHECK(f1.use_count() == 1);
        BOOST_CHECK(f2.use_count() == 1);
        BOOST_CHECK(f3.use_count() == 1);
        BOOST_CHECK(f4.use_count() == 1);
        BOOST_CHECK(f5.use_count() == 1);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);
        fm->addFlow(f4);
        fm->addFlow(f5);

        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(f4.use_count() == 2);
        BOOST_CHECK(f5.use_count() == 2);

        // The flows are not sorted on the multi_index
        f1->setLastPacketTime(150);
        f2->setLastPacketTime(110);
        f3->setLastPacketTime(12); // comatose flow
        f4->setLastPacketTime(17); // comatose flow
        f5->setLastPacketTime(140);

	// remove two flows
	fm->removeFlow(f3);
	fm->removeFlow(f5);

        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 1);
        BOOST_CHECK(f4.use_count() == 2);
        BOOST_CHECK(f5.use_count() == 1);

        BOOST_CHECK(fm->getTotalFlows() == 3);

	BOOST_CHECK(fm->findFlow(1, 0) != nullptr);
	BOOST_CHECK(fm->findFlow(22, 0) != nullptr);

        fm->updateTimers(200);

	// fm->showFlows(std::cout, 10);

        BOOST_CHECK(fc->getTotalFlows() == 1);
        BOOST_CHECK(fm->getTotalFlows() == 2);
        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 3); // last_flow_
        BOOST_CHECK(f3.use_count() == 1);
        BOOST_CHECK(f4.use_count() == 2);
        BOOST_CHECK(f5.use_count() == 1);
}

BOOST_AUTO_TEST_CASE (test06)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2));
        auto f3 = SharedPointer<Flow>(new Flow(3));

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);

        f1->setLastPacketTime(10);
        f2->setLastPacketTime(200);
        f3->setLastPacketTime(300);

	fm->setTimeout(120);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);

	BOOST_CHECK(f1.use_count() == 2);
	BOOST_CHECK(f2.use_count() == 2);
	BOOST_CHECK(f3.use_count() == 2);

        BOOST_CHECK(fm->getTotalFlows() == 3);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

        // Update the time of the flows
        fm->updateTimers(301);

	// flow1 should not exist on the fm
	auto fout = fm->findFlow(1, 0x0fffeaf);

        BOOST_CHECK(fm->getTotalFlows() == 2);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 1);
	BOOST_CHECK(f1.use_count() == 1);
	BOOST_CHECK(f2.use_count() == 2);
	BOOST_CHECK(f3.use_count() == 2);
	BOOST_CHECK(fout.use_count() == 0);
	BOOST_CHECK(fout == nullptr);
}

BOOST_AUTO_TEST_CASE (test07)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2042));
        auto f3 = SharedPointer<Flow>(new Flow(7));

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);

        f1->setLastPacketTime(100);
        f2->setLastPacketTime(20);
        f3->setLastPacketTime(300);

	fm->setTimeout(210);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);

        BOOST_CHECK(fm->getTotalFlows() == 3);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	BOOST_CHECK(fm->findFlow(1, 0) != nullptr);
	BOOST_CHECK(fm->findFlow(7, 0) != nullptr);

        // Update the time of the flows
        fm->updateTimers(301);

        auto fout = fm->findFlow(2042, 0);

        BOOST_CHECK(fm->getTotalFlows() == 2);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 1);
        BOOST_CHECK(fout.use_count() == 0);
        BOOST_CHECK(fout == nullptr);
	BOOST_CHECK(f1.use_count() == 2);
	BOOST_CHECK(f2.use_count() == 1); // last_lookup_
	BOOST_CHECK(f3.use_count() == 2);
}

// Test the flow manager and the flow cache timeouts
BOOST_AUTO_TEST_CASE (test08)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        FlowCachePtr fc = FlowCachePtr(new FlowCache());

        fc->createFlows(64);

        for (int i = 0; i < 66; ++i) {
                auto f = fc->acquireFlow();
                if (f) {
        		IPAddress addr;

        		uint32_t ipsrc = inet_addr("192.168.100.102");
        		uint32_t ipdst = inet_addr("213.2.1.8");
        		uint16_t portsrc = 2;
        		uint16_t portdst = i;
        		uint16_t proto = IPPROTO_TCP;

        		addr.setSourceAddress(ipsrc);
        		addr.setDestinationAddress(ipdst);

        		unsigned long h1 = addr.getHash(portsrc, proto, portdst);
        		unsigned long h2 = addr.getHash(portdst, proto, portsrc);

                        f->setId(h1);
			f->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);
			f->setArriveTime(0);

                        fm->addFlow(f);
                        BOOST_CHECK(fm->getTotalFlows() == i+1);
                }
        }

	// 64 flows should exists on the FlowManager
	BOOST_CHECK(fm->getTotalFlows() == 64);

	fm->setFlowCache(fc);
	fm->setTimeout(50);

	// Update the time of 33 flows
        for (int i = 0; i < 33; ++i) {
        	IPAddress addr;

        	uint32_t ipsrc = inet_addr("192.168.100.102");
        	uint32_t ipdst = inet_addr("213.2.1.8");
        	uint16_t portsrc = 2;
        	uint16_t portdst = i;
        	uint16_t proto = IPPROTO_TCP;

        	addr.setSourceAddress(ipsrc);
        	addr.setDestinationAddress(ipdst);

        	unsigned long h1 = addr.getHash(portsrc, proto, portdst);
        	unsigned long h2 = addr.getHash(portdst, proto, portsrc);

                auto f = fm->findFlow(h1, h2);
		if (f)
        		f->setLastPacketTime(50);
	}

	fm->updateTimers(80);

        BOOST_CHECK(fm->getTotalFlows() == 33);
	BOOST_CHECK(fm->getTotalProcessFlows() == 64);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 31);
        BOOST_CHECK(fc->getTotalFlows() == 31);
        BOOST_CHECK(fc->getTotalAcquires() == 64);
        BOOST_CHECK(fc->getTotalReleases() == 31);
        BOOST_CHECK(fc->getTotalFails() == 2);
}

BOOST_AUTO_TEST_SUITE_END( )

BOOST_AUTO_TEST_SUITE (flowmanager_time_no_release_flow) // test for manage the time with releasing the flows

BOOST_AUTO_TEST_CASE (test01)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow());
        auto f2 = SharedPointer<Flow>(new Flow());
	IPAddress addr;

	uint32_t ipsrc = inet_addr("56.125.100.2");
        uint32_t ipdst = inet_addr("213.200.11.87");
        uint16_t portsrc = 1000;
        uint16_t portdst = 80;
        uint16_t proto = IPPROTO_TCP;

       	addr.setSourceAddress(ipsrc);
       	addr.setDestinationAddress(ipdst);

	unsigned long h = addr.getHash(portsrc, proto, portdst);

        f1->setId(h);
	f1->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

	// other flow
	ipsrc = inet_addr("56.125.100.247");
       	addr.setSourceAddress(ipsrc);
	//	h = addr.getHash(portsrc, proto, portdst);

	f2->setId(2);
	f2->setFiveTuple(ipsrc, portsrc, proto, ipdst, portdst);

        f1->setArriveTime(0);
        f2->setArriveTime(0);

        fm->addFlow(f1);
        fm->addFlow(f2);

	// Tell the flow manager not to release the flows
	fm->setReleaseFlows(false);

       	f1->setLastPacketTime(200);
       	f2->setLastPacketTime(2);

	BOOST_CHECK(f1.use_count() == 2);
	BOOST_CHECK(f2.use_count() == 2);

        BOOST_CHECK(fm->getTotalFlows() == 2);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	BOOST_CHECK(fm->findFlow(h, 0) != nullptr);
        // Update the time of the flows
        fm->updateTimers(200);

        BOOST_CHECK(fm->getTotalFlows() == 2);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 1);
	BOOST_CHECK(f1.use_count() == 3); // lookup_flow_
	BOOST_CHECK(f2.use_count() == 2);
}

BOOST_AUTO_TEST_CASE (test02)
{
  	FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
	auto f1 = SharedPointer<Flow>(new Flow(1));
	auto f2 = SharedPointer<Flow>(new Flow(2));
	auto f3 = SharedPointer<Flow>(new Flow(3));

	f1->setArriveTime(0);
	f2->setArriveTime(0);
	f3->setArriveTime(0);

	f1->setLastPacketTime(1);
	f2->setLastPacketTime(2);
	f3->setLastPacketTime(200);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);

	BOOST_CHECK(f1.use_count() == 2);
	BOOST_CHECK(f2.use_count() == 2);
	BOOST_CHECK(f3.use_count() == 2);

	BOOST_CHECK(fm->getTotalFlows() == 3);
	BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

       	f1->setLastPacketTime(1);
       	f2->setLastPacketTime(2);
       	f3->setLastPacketTime(200);

	// dont release the network flows
	fm->setReleaseFlows(false);

	// Update the time of the flows
	fm->updateTimers(200);

	BOOST_CHECK(fm->getTotalFlows() == 3);
	BOOST_CHECK(fm->getTotalTimeoutFlows() == 2);

	BOOST_CHECK(f1.use_count() == 2);
	BOOST_CHECK(f2.use_count() == 2);
	BOOST_CHECK(f3.use_count() == 2);
}

BOOST_AUTO_TEST_CASE (test03)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2));
        auto f3 = SharedPointer<Flow>(new Flow(3));
        auto f4 = SharedPointer<Flow>(new Flow(4));

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);
        f4->setArriveTime(0);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);
        fm->addFlow(f4);

        //fm->showFlowsByTime();

        f1->setLastPacketTime(1);
        f2->setLastPacketTime(200);
        f3->setLastPacketTime(2);
        f4->setLastPacketTime(210);

        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(f4.use_count() == 2);

        BOOST_CHECK(fm->getTotalFlows() == 4);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	// Dont release the flows
	fm->setReleaseFlows(false);

	// Generate the move of the flows to the top
	BOOST_CHECK(fm->findFlow(2, 0) != nullptr);
	BOOST_CHECK(fm->findFlow(4, 0) != nullptr);

        // Update the time of the flows
        fm->updateTimers(220);

        BOOST_CHECK(fm->getTotalFlows() == 4);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 2);
        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(f4.use_count() == 3); // lookup_flow_
}

BOOST_AUTO_TEST_CASE (test04)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2));
        auto f3 = SharedPointer<Flow>(new Flow(3));

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);

        f1->setLastPacketTime(10);
        f2->setLastPacketTime(200);
        f3->setLastPacketTime(300);

        fm->setTimeout(120);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);

        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);

        BOOST_CHECK(fm->getTotalFlows() == 3);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	fm->setReleaseFlows(false);

        // Update the time of the flows
        fm->updateTimers(301);

        // flow1 should not exist on the fm
        auto fout = fm->findFlow(1, 0x0fffeaf);

        BOOST_CHECK(fm->getTotalFlows() == 3);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 1);
        BOOST_CHECK(f1.use_count() == 4);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(fout.use_count() == 4);
        BOOST_CHECK(fout == f1);
}

BOOST_AUTO_TEST_CASE (test05)
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2042));
        auto f3 = SharedPointer<Flow>(new Flow(7));

        f1->setArriveTime(0);
        f2->setArriveTime(0);
        f3->setArriveTime(0);

        f1->setLastPacketTime(100);
        f2->setLastPacketTime(20);
        f3->setLastPacketTime(300);

        fm->setTimeout(210);

        fm->addFlow(f1);
        fm->addFlow(f2);
        fm->addFlow(f3);

        BOOST_CHECK(fm->getTotalFlows() == 3);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	fm->setReleaseFlows(false);

	BOOST_CHECK(fm->findFlow(1, 0) != nullptr);
	BOOST_CHECK(fm->findFlow(0, 7) != nullptr);
        // Update the time of the flows
        fm->updateTimers(301);

        auto fout = fm->findFlow(2042, 0);

        BOOST_CHECK(fm->getTotalFlows() == 3);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 1);
        BOOST_CHECK(fout.use_count() == 4);
        BOOST_CHECK(fout == f2);
}

BOOST_AUTO_TEST_CASE (test06) // verify the method flush with a protocol
{
        FlowManagerPtr fm = FlowManagerPtr(new FlowManager());
        auto f1 = SharedPointer<Flow>(new Flow(1));
        auto f2 = SharedPointer<Flow>(new Flow(2042));
        auto f3 = SharedPointer<Flow>(new Flow(70));
        auto f4 = SharedPointer<Flow>(new Flow(107));

	fm->addFlow(f1);
	fm->addFlow(f2);
	fm->addFlow(f3);
	fm->addFlow(f4);

	// All the flows have "None" on their name, there is no FlowForwarder
        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(f4.use_count() == 2);
        BOOST_CHECK(fm->getTotalFlows() == 4);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	fm->flush("nothing");

        BOOST_CHECK(f1.use_count() == 2);
        BOOST_CHECK(f2.use_count() == 2);
        BOOST_CHECK(f3.use_count() == 2);
        BOOST_CHECK(f4.use_count() == 2);
        BOOST_CHECK(fm->getTotalFlows() == 4);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);

	fm->flush("none");

        BOOST_CHECK(f1.use_count() == 1);
        BOOST_CHECK(f2.use_count() == 1);
        BOOST_CHECK(f3.use_count() == 1);
        BOOST_CHECK(f4.use_count() == 1);
        BOOST_CHECK(fm->getTotalFlows() == 0);
        BOOST_CHECK(fm->getTotalTimeoutFlows() == 0);
}


BOOST_AUTO_TEST_SUITE_END( )
