/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2021  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "FlowTable.h"
#include <iomanip> // setw

namespace aiengine {

void FlowTable::detach(FlowNode *node) {

	if (node == head) {
		head = node->next;
		if (tail == node)
			tail = nullptr;
		if (head)
			head->prev = nullptr;
	} else {
		if (node == tail) {
			node->prev->next = nullptr;
			tail = tail->prev;
		} else {
			node->prev->next = node->next;
			node->next->prev = node->prev;
		}
	}
}

void FlowTable::attach(FlowNode *node) {

	if ((head == nullptr)and(tail == nullptr)) {
		head = tail = node;
	} else {
		node->next = head;
		node->prev = nullptr;
		node->next->prev = node;
		head = node;
	}
}

} // namespace aiengine
